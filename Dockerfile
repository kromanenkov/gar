FROM ubuntu:16.04

RUN apt-get update && apt-get install -y wget g++ python python3 git make cpio

# Get NCBI BLAST+ package.
RUN wget -P /tmp/ ftp://ftp.ncbi.nlm.nih.gov/blast/executables/blast+/2.6.0/ncbi-blast-2.6.0+-src.tar.gz &&\
    mkdir -p /main/blast+-src && tar -xvzf /tmp/ncbi-blast-2.6.0+-src.tar.gz \
    -C /main/blast+-src && rm /tmp/ncbi-blast-2.6.0+-src.tar.gz

# Build NCBI BLAST+ package.
WORKDIR /main/blast+-src/ncbi-blast-2.6.0+-src/c++/
RUN mkdir /main/blast+-install/ && ./configure &&\
    cd ReleaseMT/build && make -j4 all_r

# Get Google sparsehash package.
RUN git clone https://github.com/sparsehash/sparsehash.git /main/sparsehash

# Build Google sparsehash package.
RUN cd /main/sparsehash && mkdir /main/sparsehash-install &&\
    ./configure --prefix=/main/sparsehash-install && make && make install

COPY scripts /main/scripts
COPY src /main/src

# Patch Makefile.
RUN sed -i 's;/path/to/sparsehash;/main/sparsehash-install/;' \
        /main/src/Makefile
RUN sed -i 's;/path/to/blast+;/main/blast+-src/ncbi-blast-2.6.0+-src/;' \
        /main/src/Makefile

# Patch makeBlastDbs.sh.
RUN sed -i 's;^scripts_path.*$;scripts_path=/main/scripts;' \
        /main/scripts/makeBlastDbs.sh
RUN sed -i 's;^mkdb_path.*$;mkdb_path=/main/blast+-src/ncbi-blast-2.6.0+-src/c++/ReleaseMT/bin/;' \
        /main/scripts/makeBlastDbs.sh

# Build gar.
RUN mkdir -p /main/bin/ && cd /main/src && make -j4

# Add executables to PATH.
ENV PATH="/main/bin:/main/scripts/:${PATH}"

WORKDIR /main/bin

#GAR - Genome Assembly Refinement

Tool for refinement contig sets obtained from different genome assemblers.

GAR is linked with [NCBI BLAST+](ftp://ftp.ncbi.nlm.nih.gov/blast/executables/blast+/LATEST/) package and [Google sparsehash](https://github.com/sparsehash/sparsehash).
Both of them should be built separately from source before GAR compilation. BLAST+
building instructions could be found here: https://www.ncbi.nlm.nih.gov/books/NBK279671/#introduction.Source_tarball. Google sparsehash building instructions could be found here: https://github.com/sparsehash/sparsehash/blob/master/INSTALL. Please note that you need not only build but also install sparsehash library, so GAR could find all headers in one place.

After these steps you need to specify the path to the NCBI BLAST+ source directory as well as the path
to the google sparsehash installation directory in the GAR's Makefile (.../src/Makefile) and type make. This will generate gar executable in .../bin/ directory. Please, also correct paths to makeblastdb and scripts in .../scripts/makeBlastDbs.sh script.

Before running GAR you need to collect all files with assemblies in one folder, index them and create an output folder. So typical session could be as follows:
```bash
mkdir working_dir
cp assembly1.fa assembly2.fa assembly3.fa ./working_dir/
cd working_dir
/path/to/gar/scripts/makeBlastDbs.sh -parse_seqid
mkdir Out
/path/to/gar/bin/gar --indir=./
```

GAR will place the resulting assembly in working_dir/Out/gar.fa




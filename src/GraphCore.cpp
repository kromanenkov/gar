#include "include/GraphCore.h"
#include "include/GraphSplitterType.h"

// NOTE: This function exists for debug purposes
void dumpLabels(const LabelsMap &LM) {
  LabelsMap::const_iterator I = LM.begin(), E = LM.end();
  cout << "///// Labels /////\n";
  while (I != E) {
    cout << "(" << I->first << ", " << I->second << "); ";
    ++I;
  }
  cout << "\n";
}

// NOTE: This function exists for debug purposes
void dumpNeighbors(const AdjMap &AM) {
  AdjMap::const_iterator I = AM.begin(), E = AM.end();
  cout << "||||| Neighbors |||||\n(";
  while (I != E) {
    cout << "(" << I->first << ", " << I->second->bitScore << ", "
         << I->second->eValue << "); ";
    ++I;
  }
  cout << ")\n";
}

template<typename T>
inline std::istream& operator>>(std::istream &str, T &CustomParam) {
  unsigned int LocalCustomParam = 0;
  if (str >> LocalCustomParam)
    CustomParam = static_cast<T>(LocalCustomParam);
  return str;
}

//TODO: write fair binary I/O
bool SparseMatrixSerializer::operator()
    (ofstream* ofile, const pair<BlastScores::PairId, EdgeInfo>& value) const {
  (*ofile) << value.first.first << " " << value.first.second << " " << value.second.ET << " "
      << value.second.ST << " " << value.second.bitScore << " " << value.second.numIdent << " "
      << value.second.eValue << " " << value.second.seq1start << " " << value.second.seq1end << " "
      << value.second.seq2start << " " << value.second.seq2end << " "
      << value.second.alignlength << " ";
  return true;
}

//TODO: write fair binary I/O
bool SparseMatrixSerializer::operator()
    (ifstream* infile, pair<const BlastScores::PairId, EdgeInfo>* value) const {
  (*infile) >> const_cast<int&>(value->first.first) >> const_cast<int&>(value->first.second)
      >> const_cast<EdgeInfo::EdgeType&>(value->second.ET) >> const_cast<EdgeInfo::StrandType&>(value->second.ST)
      >> const_cast<double&>(value->second.bitScore) >> const_cast<int&>(value->second.numIdent)
      >> const_cast<double&>(value->second.eValue) >> const_cast<unsigned int&>(value->second.seq1start)
      >> const_cast<unsigned int&>(value->second.seq1end) >> const_cast<unsigned int&>(value->second.seq2start)
      >> const_cast<unsigned int&>(value->second.seq2end) >> const_cast<unsigned int&>(value->second.alignlength);
  return true;
}

int GraphBuilder::allocScoreMatrix() {
  if (!setsNum) return -1;

  scoreMatrix.matrix.resize(setsNum);
  for (unsigned int i = 0; i < setsNum; ++i) {
    scoreMatrix.matrix[i].resize(i);
    //contigs inside one set are never compared with each other
    for (unsigned int j = 0; j < i; ++j) {
      scoreMatrix.matrix[i][j] = BlastScores::SparseMatrix();
    }
  }
  return 1;
}

void GraphBuilder::fillScoreMatrix(
    const ContigsBank::ContigsSetsTy &contigsSets, const string indirname,
    const vector<string>& setsnames, const vector<float>& avcontiglens) {
  vector<string> setsfullnames;
  for (unsigned i = 0; i < setsnames.size(); ++i)
    setsfullnames.push_back(indirname + setsnames[i]);
  if (allocScoreMatrix() == -1 ||
      blastRunner->initScopes(setsfullnames) == -1) {
    cout << "Problems with memory allocation or BlastRunner\n";
    return;
  }

  timeval tv;
  gettimeofday(&tv, NULL);
  __time_t startsec = tv.tv_sec;
  double ts = omp_get_wtime();

  for (unsigned int i = 0; i < setsNum; ++i)
    for (unsigned int j = 0; j < i; ++j) {
      vector<EdgeInfoWithSetsNums> *resultVector =
          blastRunner->makeMBlast(i, j, avcontiglens);
      if (!resultVector)
        continue;

      for (vector<EdgeInfoWithSetsNums>::const_iterator
             resI = resultVector->begin(),
             resE = resultVector->end(); resI != resE;++resI) {
        BlastScores::PairId key = make_pair(resI->seq1id, resI->seq2id);
        assert(resI->set2id >= 0 && i > static_cast<unsigned int>(resI->set2id));
        scoreMatrix.matrix[i][resI->set2id][key] = resI->eInfo;
      }
      resultVector->clear();
      delete resultVector;
      cout << "\rProcessed " << ((i * i - i) >> 1) + j + 1 << "/" << setsNum * (setsNum - 1) / 2 << " sets pairs";
      cout.flush();
    }
  cout << "\n";

  gettimeofday(&tv, NULL);
  cout << "\nIt tooks " << tv.tv_sec - startsec << " seconds.\n";
  cout << "Tooks " << omp_get_wtime() - ts << ".\n";
  delete blastRunner;
}

void GraphBuilder::deallocScoreMatrix() {
  for (unsigned int i = 0; i < setsNum; ++i) {
    //contigs inside one set are never compared with each other
    for (unsigned int j = 0; j < i; ++j) {
      //TODO: Isn't just clear() enough?
      scoreMatrix.matrix[i][j].clear();
      scoreMatrix.matrix[i][j].resize(0);
    }
    scoreMatrix.matrix[i].clear();
  }
  scoreMatrix.matrix.clear();
}

GraphBuilder::~GraphBuilder() {
  deallocScoreMatrix();
}

bool Vertex::hasInboundExtension(
    const Vertex &v, const EdgeInfo* EI,
    const ContigsBank::ContigsSetsTy &contigsSets) const {
  bool Seq1Start = EI->seq1start == 0,
       Seq2Start = EI->seq2start == 0;

  if (setId < v.setId) { // v, c2 -> seq1, this, c1 -> seq2
    return Seq2Start;
  } else { // (v.setId < this->setId) as this->setId != v.setId
    // v, c2 -> seq2, this, c1 -> seq1
    return Seq1Start;
  }
}

bool Vertex::hasOutboundExtension(
    const Vertex &v, const EdgeInfo *EI,
    const ContigsBank::ContigsSetsTy &contigsSets) const {
  const Contig &c1 = contigsSets[setId][contigId];

  bool Seq1EndC1 = EI->seq1end == (c1.getLen() - 1),
       Seq2EndC1 = EI->seq2end == (c1.getLen() - 1);

  if (setId < v.setId) { // v, c2 -> seq1, this, c1 -> seq2
    return Seq2EndC1;
  } else { // (v.setId < this->setId) as this->setId != v.setId
    // v, c2 -> seq2, this, c1 -> seq1
    return Seq1EndC1;
  }
}

int GraphBuilder::dumpScoreMatrix(string outPred) {
  std::string DirWithScores = outPred.substr(0, outPred.rfind('/'));

  cout << "Dumping score matrices to " << DirWithScores << "...\n";

  for (unsigned int i = 0; i < setsNum; ++i)
    for (unsigned int j = 0; j < i; ++j) {
      stringstream seti, setj;
      seti << i; setj << j;
      ofstream ofile((outPred + seti.str() + "_" + setj.str() +
                      ".data").c_str(), ofstream::binary);
      if (!ofile.is_open()) {
        cout << "Can't write to file "
             << outPred + seti.str() + "_" + setj.str() + ".data" << "\n";
        cout << "Unexpected problems during score matrix dumping.\n";
        return -1;
      }
      scoreMatrix.matrix[i][j].serialize(SparseMatrixSerializer(), &ofile);
      ofile.close();
    }
  cout << "Dumping is finished.\n";
  return 1;
}

int GraphBuilder::loadScoreMatrix(string inPred) {
  std::string DirWithScores = inPred.substr(0, inPred.rfind('/'));

  cout << "Loading score matrices from " << DirWithScores << "...\n";

  for (unsigned int i = 0; i < setsNum; ++i)
    for (unsigned int j = 0; j < i; ++j) {
      stringstream seti, setj;
      seti << i; setj << j;
      std::string ScoreFileName = inPred + seti.str() + "_" + setj.str() +
                                  ".data";

      ifstream ifile(ScoreFileName.c_str(), ifstream::binary);
      if (!ifile.is_open()) {
        cout << "Can't read from file "
             << ScoreFileName << "\n";
        cout << "Unexpected problems during score matrix loading. "
             << "GAR is terminated.\n";
        return -1;
      }
      if (!scoreMatrix.matrix[i][j].unserialize(SparseMatrixSerializer(),
                                                &ifile)) {
        cout << "Unexpected problems during unserializing score matrix from "
             << ScoreFileName << ".\n";
        ifile.close();
        return -1;
      }
      ifile.close();
    }

  cout << "Loading is finished.\n";
  return 1;
}

int GraphBuilder::buildGraph(const ContigsBank::ContigsSetsTy &contigsSets) {
  for (unsigned int i = 0; i < setsNum; ++i)
    for (unsigned int j = 0; j < i; ++j) {
      // FIXME: Fix compile error when use const_iterator.
      for (BlastScores::SparseMatrix::iterator
               I = scoreMatrix.matrix[i][j].begin(),
               E = scoreMatrix.matrix[i][j].end(); I != E; ++I) {
        std::pair<int, int> key1 = make_pair(i, I->first.first),
                            key2 = make_pair(j, I->first.second);
        ContigGraph::Vertex2IdMap::const_iterator
            I1 = cGraph.vrtx2Id.find(key1), I2,
            CurrentEndIter = cGraph.vrtx2Id.end();

        long int id1, id2, vrtxnum = cGraph.vertexNum;
        int increment = 0;

        if (I1 == CurrentEndIter) {
          //add vertex
          cGraph.vertexes.push_back(Vertex(i, I->first.first));
          id1 = vrtxnum;
          ++increment;
          cGraph.vrtx2Id[key1] = id1;
          CurrentEndIter = cGraph.vrtx2Id.end();
        } else id1 = I1->second;

        I2 = cGraph.vrtx2Id.find(key2);
        if (I2 == CurrentEndIter) {
          //add vertex
          cGraph.vertexes.push_back(Vertex(j, I->first.second));
          id2 = vrtxnum + increment;
          ++increment;
          cGraph.vrtx2Id[key2] = id2;
        } else id2 = I2->second;

        if (increment)
          cGraph.vertexNum += increment;

        if (id1 >= cGraph.vertexNum || id2 >= cGraph.vertexNum) {
          cout << "buildGraph: Invalid vertex id!\n";
          return -1;
        }

        Vertex &v1 = cGraph.vertexes[id1];
        Vertex &v2 = cGraph.vertexes[id2];

        //add edge
        switch (I->second.ET) {
          case EdgeInfo::Normal: {
            v1.adjMap[id2] = &(I->second);
            v2.adjMap[id1] = &(I->second);
            if (v1.hasInboundExtension(v2, &(I->second), contigsSets)) {
              v1.adjMap.InboundNeighbors.push_back(id2);
              if (EdgeInfo::strandIsChanged(I->second.ST))
                v2.adjMap.InboundNeighbors.push_back(id1);
              else
                v2.adjMap.OutboundNeighbors.push_back(id1);
            } else if (v1.hasOutboundExtension(v2, &(I->second), contigsSets)) {
              v1.adjMap.OutboundNeighbors.push_back(id2);
              if (EdgeInfo::strandIsChanged(I->second.ST))
                v2.adjMap.OutboundNeighbors.push_back(id1);
              else
                v2.adjMap.InboundNeighbors.push_back(id1);
            }
            break;
          }
          case EdgeInfo::FirstCovers: {
            v1.coveredVIdMap[id2] = &(I->second);
            v2.parentVIdMap[id1] = &(I->second);
            break;
          }
          case EdgeInfo::SecondCovers: {
            v2.coveredVIdMap[id1] = &(I->second);
            v1.parentVIdMap[id2] = &(I->second);
            break;
          }
          case EdgeInfo::Identical: {
            v1.coveredVIdMap[id2] = &(I->second);
            v2.coveredVIdMap[id1] = &(I->second);
            break;
          }
          case EdgeInfo::PartialCoverage: {
            v1.partlyCoveredVIdMap[id2] = &(I->second);
            v2.partlyCoveredVIdMap[id1] = &(I->second);
            break;
          }
          default: {
            cout << "buildGraph: Invalid EdgeType(" << I->second.ET << ")!\n";
            return -1;
          }
        }
      }
    }

  for (long int i = 0; i < cGraph.vertexNum; ++i) {
    Vertex &V = cGraph.vertexes[i];

    V.neighborNum = static_cast<long int>(V.adjMap.size());
    V.coveredNum = V.coveredVIdMap.size();
    V.adjMap.InboundNum = V.adjMap.InboundNeighbors.size();
    V.adjMap.OutboundNum = V.adjMap.OutboundNeighbors.size();

    if (V.adjMap.InboundNum + V.adjMap.OutboundNum != V.neighborNum) {
      cout << "Sum of Inbound and Outbound neighbors is not equal neighbor "
           << "num!\n";
      cout << "Inbound=" << V.adjMap.InboundNum << "; Outbound="
           << V.adjMap.OutboundNum << "; Sum=" << V.neighborNum << "\n";
      dumpNeighbors(V.adjMap);
    }

    assert(V.adjMap.InboundNum + V.adjMap.OutboundNum == V.neighborNum);
  }

  return 1;
}

template <class WeightPolicy>
void LabelRank<WeightPolicy>::initializeLabels() {
  // Alias for vertexes.
  std::vector<Vertex> &VertRef = this->cg.vertexes;

  for (long int i = 0; i < this->cg.vertexNum; ++i) {
    if (VertRef[i].isIsolated())
      continue;

    // It turns out that selfloop improve quality of clustering.
    double InitDistrib = 1.0 / (VertRef[i].neighborNum + 1);
    double EdgeWeightsSum = 0.0;

    if (VertRef[i].isIsolatedWithInclusions()) {
      VertRef[i].prevLabels[i] = InitDistrib;
      continue;
    }
    for (ConstNghbrIter I = VertRef[i].adjMap.begin(),
                        E = VertRef[i].adjMap.end(); I != E; ++I) {
      VertRef[i].prevLabels[I->first] = InitDistrib;
      EdgeWeightsSum += weightPolicy.getScore(I->second);
    }
    VertRef[i].prevLabels[i] = InitDistrib;
    VertRef[i].totalEdgeWeight = EdgeWeightsSum;

    // Add self-loop to vertex.
    EdgeInfo *EI = new EdgeInfo();
    // FIXME: keep it synced with WeightPolicy
    EI->bitScore = EdgeWeightsSum / VertRef[i].neighborNum;
    VertRef[i].adjMap[i] = EI;

    VertRef[i].totalEdgeWeight += EI->bitScore;
  }
}

template <typename WeightPolicy>
bool LabelRank<WeightPolicy>::stopCondition() {
  if (changeNum == 0 || (++updatedVertexesNum[changeNum] > stopFreq))
    return true;
  return false;
}

template <typename WeightPolicy>
void LabelRank<WeightPolicy>::propagateOperation() {
  typedef dense_hash_set<long int> ActiveLabelsSet;
  // alias for graph vertexes
  std::vector<Vertex> &VertRef = this->cg.vertexes;

  // vertexes cycle
  for (long int i = 0; i < this->cg.vertexNum; ++i) {
    if (VertRef[i].isIsolated())
      continue;

    ActiveLabelsSet ActiveLabels;
    ActiveLabels.set_empty_key(-1);

    // form set of labels stored by vertex's neighbors
    for (ConstNghbrIter NI = VertRef[i].adjMap.begin(),
                        NE = VertRef[i].adjMap.end();
         NI != NE; ++NI)
      for (ConstVertexLabelIter VLI = VertRef[NI->first].prevLabels.begin(),
                                VLE = VertRef[NI->first].prevLabels.end();
           VLI != VLE; ++VLI)
        ActiveLabels.insert(VLI->first);
    // labels cycle
    for (ActiveLabelsSet::const_iterator ALI = ActiveLabels.begin(),
                                         ALE = ActiveLabels.end();
         ALI != ALE; ++ALI) {
      double lblcoeff = 0.0;
      // neighbor cycle
      for (ConstNghbrIter NI = VertRef[i].adjMap.begin(),
                          NE = VertRef[i].adjMap.end();
           NI != NE; ++NI) {
        const LabelsMap &NeighborLMRef =
            VertRef[NI->first].prevLabels;
        ConstVertexLabelIter NVLI = NeighborLMRef.find(*ALI);

        lblcoeff += weightPolicy.getScore(NI->second) *
                    ((NVLI == NeighborLMRef.end())? 0.0 : NVLI->second);
      }
      VertRef[i].currLabels[*ALI] = lblcoeff;
    }
  }

  // verify method
  for (long int i = 0; i < this->cg.vertexNum; ++i) {
    double CheckSum = 0.0;
    for (ConstVertexLabelIter I = VertRef[i].currLabels.begin(),
                              E = VertRef[i].currLabels.end();
         I != E; ++I)
      CheckSum += I->second;
    assert(fabs(CheckSum - VertRef[i].totalEdgeWeight) < CLUSTERING_EPS);
  }
}

template <typename WeightPolicy>
void LabelRank<WeightPolicy>::inflationOperation() {
  // alias for graph vertexes
  std::vector<Vertex> &VertRef = this->cg.vertexes;

  for (long int i = 0; i < this->cg.vertexNum; ++i) {
    if (VertRef[i].isIsolated())
      continue;

    double labelssum = 0.0;

    for (VertexLabelIter VLI = VertRef[i].currLabels.begin(),
                         VLE = VertRef[i].currLabels.end(); VLI != VLE; ++VLI) {
      double inflcoeff = std::pow(VLI->second, inflationPower);
      labelssum += (VLI->second = inflcoeff);
    }
    for (VertexLabelIter VLI = VertRef[i].currLabels.begin(),
                         VLE = VertRef[i].currLabels.end(); VLI != VLE; ++VLI)
      VLI->second /= labelssum;

    //verify method
    double sum = 0.0;
    for (ConstVertexLabelIter I = VertRef[i].currLabels.begin(),
                              E = VertRef[i].currLabels.end();
         I != E; ++I)
      sum += I->second;
    assert(fabs(1. - sum) < CLUSTERING_EPS && "Inflation");
  }
}

template <typename WeightPolicy>
void LabelRank<WeightPolicy>::cutoffOperation() {
  // alias for graph vertexes
  std::vector<Vertex> &VertRef = this->cg.vertexes;

  //control method
  //dumpLabels(VertRef[0].currLabels);

  for (long int i = 0; i < this->cg.vertexNum; ++i) {
    if (VertRef[i].isIsolated())
      continue;

    LabelsMap MaxLabelsI;
    MaxLabelsI.set_empty_key(-1);
    MaxLabelsI.set_deleted_key(-2);

    MaxLabelsI = getMaxLabels(VertRef[i].currLabels);

    if (MaxLabelsI.begin()->second < threshold) {
      // all label coeffs are less than threshold
      VertRef[i].currLabels.clear();
      VertRef[i].currLabels.rehash(0);
      VertRef[i].currLabels.swap(MaxLabelsI);
    } else {
      // exist at least one label coeff more than threshold
      for (VertexLabelIter VLI = VertRef[i].currLabels.begin(),
                           VLE = VertRef[i].currLabels.end();
           VLI != VLE; ++VLI)
        if (VLI->second < threshold) VertRef[i].currLabels.erase(VLI);

      VertRef[i].currLabels.rehash(0);
    }
  }

  //control method
  //dumpLabels(VertRef[0].currLabels);
}

template <typename WeightPolicy>
const LabelsMap LabelRank<WeightPolicy>::getMaxLabels(const LabelsMap& lm) {
  double maxprob = DBL_MIN;
  LabelsMap MaxLabels;
  MaxLabels.set_empty_key(-1);
  MaxLabels.set_deleted_key(-2);

  for (ConstVertexLabelIter VLI = lm.begin(), VLE = lm.end();
       VLI != VLE; ++VLI)
    if (VLI->second > maxprob) maxprob = VLI->second;
  for (ConstVertexLabelIter VLI = lm.begin(), VLE = lm.end();
       VLI != VLE; ++VLI)
    if (fabs(VLI->second - maxprob) < CLUSTERING_EPS)
      MaxLabels.insert(*VLI);

  return MaxLabels;
}

template <typename WeightPolicy>
void LabelRank<WeightPolicy>::normalizeLabels(LabelsMap& lm) {
  double labelssum = 0.0;

  for (ConstVertexLabelIter VLI = lm.begin(), VLE = lm.end();
       VLI != VLE; ++VLI)
    labelssum += VLI->second;
  for (VertexLabelIter VLI = lm.begin(), VLE = lm.end();
       VLI != VLE; ++VLI)
    VLI->second /= labelssum;
}

template <typename WeightPolicy>
void LabelRank<WeightPolicy>::conditionalUpdateOperation() {
  // alias for graph vertexes
  std::vector<Vertex> &VertRef = this->cg.vertexes;

  for (long int i = 0; i < this->cg.vertexNum; ++i) {
    if (VertRef[i].isIsolated())
      continue;

    int similarnum = 0;
    LabelsMap MaxLabelsI;
    MaxLabelsI.set_empty_key(-1);
    MaxLabelsI = getMaxLabels(VertRef[i].prevLabels);

    for (ConstNghbrIter NI = VertRef[i].adjMap.begin(),
                        NE = VertRef[i].adjMap.end();
         NI != NE; ++NI) {
      if (i == NI->first)
        continue;

      LabelsMap MaxLabelsNI;
      MaxLabelsNI.set_empty_key(-1);

      MaxLabelsNI = getMaxLabels(VertRef[NI->first].prevLabels);
      similarnum += ((isSubset(MaxLabelsI, MaxLabelsNI))? 1 : 0);
    }
    if (similarnum > VertRef[i].neighborNum * updateFreq) {
      // don't accept the modified label distribution
      VertRef[i].currLabels.clear();
      VertRef[i].currLabels.rehash(0);
      VertRef[i].currLabels = VertRef[i].prevLabels;

      // verify method
      assert(VertRef[i].currLabels.size() == VertRef[i].prevLabels.size());
      for (ConstVertexLabelIter VLI = VertRef[i].currLabels.begin(),
                                VLE = VertRef[i].currLabels.end();
           VLI != VLE; ++VLI) {
        assert(VLI->second == VertRef[i].prevLabels[VLI->first]);
      }
    } else
      //update vertex distribution
      ++changeNum;

    normalizeLabels(VertRef[i].currLabels);
  }
}

template<typename WeightPolicy>
void LabelRank<WeightPolicy>::deleteSelfLoops() {
  std::vector<Vertex> &VertRef = this->cg.vertexes;

  for (long int i = 0; i < this->cg.vertexNum; ++i) {
    delete VertRef[i].adjMap[i];
    VertRef[i].adjMap.erase(i);
    // TODO: Consider simply marking key as deleted without rehashing.
    VertRef[i].adjMap.rehash(0);
  }
}

template <typename WeightPolicy>
int LabelRank<WeightPolicy>::runClustering() {
  // alias for graph vertexes
  std::vector<Vertex> &VertRef = this->cg.vertexes;
  int iter = 0;

  do {
    //verify method
    for (long int j = 0; j < this->cg.vertexNum; ++j) {
      if (VertRef[j].isIsolated())
        continue;

      double sum = 0.0;
      for (ConstVertexLabelIter I = VertRef[j].prevLabels.begin(),
                                E = VertRef[j].prevLabels.end();
           I != E; ++I)
        sum += I->second;
      assert(fabs(1. - sum) < CLUSTERING_EPS && "Labels aren't normalized");
    }

    changeNum = 0;
    propagateOperation();
    inflationOperation();
    cutoffOperation();
    conditionalUpdateOperation();

    for (long int i = 0; i < this->cg.vertexNum; ++i) {
      if (VertRef[i].isIsolated())
        continue;

      VertRef[i].prevLabels.swap(VertRef[i].currLabels);
      VertRef[i].currLabels.clear();
      VertRef[i].currLabels.resize(0);
    }
    cout << "\rIter #" << ++iter;
    cout.flush();

  } while (!stopCondition());
  cout << "\n";

  // Restore the original graph structure.
  deleteSelfLoops();

  // fill clusterLabels structure (cluster id -> set of vertexes id)
  for (long int i = 0; i < this->cg.vertexNum; ++i) {
    if (VertRef[i].isIsolated())
      continue;

    LabelsMap LabelsMaxI = getMaxLabels(VertRef[i].prevLabels);
    long int clustid = LabelsMaxI.begin()->first;

    if (LabelsMaxI.size() > 1) {
      for (ConstVertexLabelIter I = LabelsMaxI.begin(),
                                E = LabelsMaxI.end();
           I != E; ++I) {
        map< long int, set<long int> >::iterator VI =
            this->clusterLabels.find(I->first);
        if (VI != this->clusterLabels.end()) {
          clustid = VI->first;
          break;
        }
      }
    }
    this->clusterLabels[clustid].insert(i);
    VertRef[i].clusterId = clustid;
  }
  this->clustersNum = this->clusterLabels.size();
  cout << "ClusterNum = " << this->clustersNum << "\n";

  return iter;
}

// explicit template instantiation to avoid linking errors
template class LabelRank<EdgeWeightCounter>;

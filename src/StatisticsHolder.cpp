#include "include/StatisticsHolder.h"

void StatisticsHolder::printInputStats(
    const vector <vector<Contig> > &contigsSets) {
  int size = setsSizes.size();
  if (size == 0) return;
  float av, max, min;

  for (int i = 0; i < size; ++i) {
    min = FLT_MAX; av = 0; max = -1;
    cout << "Contigs set #" << i << "(" << setsNames[i] << "): "
         << setsSizes[i] << " sequences; ";
    for (unsigned j = 0; j < contigsSets[i].size(); ++j) {
      long len = contigsSets[i][j].getLen();
      av += len;
      if (len < min) min = len;
      if (len > max) max = len;
    }
    summaryInputLens[i] = static_cast<long>(av);
    avInputLens[i] = av / setsSizes[i];
    maxInputLens[i] = max;
    minInputLens[i] = min;
    if (needDetails)
      cout << "Av = " << av / setsSizes[i] << "; Min len = " << min
           << "; Max len = " << max << ";";
    cout << "\n\n";
  }
}

void StatisticsHolder::printScoreMatrixStats(
    const vector<vector<BlastScores::SparseMatrix> > &matr) {
  if (!needDetails)
    return;

  int size = setsSizes.size();
  if (size == 0) return;

  for (int i = 0; i < size; ++i)
    for (int j = 0; j < i; ++j) {
      cout << "Set " << i << " - Set " << j << ": Non-zero elems = "
           << matr[i][j].size() << "\n";
    }
}

void StatisticsHolder::dumpGraphVertex(const Vertex& V, long int vertexId,
                                       ofstream& outFile) {
  outFile << vertexId << " [label=" << "\"(" << V.setId << ";" << V.contigId
          << ")\"" << "]; ";
}

void StatisticsHolder::dumpContigGraph(
    const ContigGraph &contigGraph, const vector< vector<Contig> > &contigsSets,
    const std::string &outDirName) {
  const vector<Vertex> &AdjVect = contigGraph.vertexes;
  ofstream DotFile((outDirName + "contigGraph.dot").c_str());

  // Nodes
  DotFile << "graph contig_graph {\n";
  for (unsigned i = 0, ie = contigsSets.size(); i != ie ; ++i) {
    DotFile << "{ rank = same; ";
    for (unsigned j = 0, je = contigsSets[i].size(); j != je; ++j) {
      ContigGraph::Vertex2IdMap::const_iterator I =
          contigGraph.vrtx2Id.find(make_pair(i, j));
      // Skip contigs without any overlapping
      if (I == contigGraph.vrtx2Id.end())
        continue;

      long int VertexId = I->second;
      dumpGraphVertex(AdjVect[VertexId], VertexId, DotFile);
    }
    DotFile << "};\n";
  }

  // Edges
  DotFile << "\n";
  for (long i = 0; i < contigGraph.vertexNum; ++i) {
    DotFile << i << " -- { ";
    for (AdjMap::const_iterator I = AdjVect[i].adjMap.begin(),
                                E = AdjVect[i].adjMap.end(); I != E; ++I)
      DotFile << I->first << "; ";
    DotFile << "}\n";
  }
  DotFile << "}\n";
}

void StatisticsHolder::printContigGraphStats(const ContigGraph &contigGraph,
    const ContigsBank::ContigsSetsTy &contigsSets,
    const std::string &outDirName) {
  long int EdgeNum = 0, NormalNum = 0, IsolatedWithoutInclusionNum = 0,
           IsolatedWithInclusionsNum = 0;
  const vector<Vertex> &AdjVect = contigGraph.vertexes;
  long int MaxDegree = 0, MinDegree = std::numeric_limits<long int>::max();
  unsigned long int MaxInclusions = 0, InclusionsNum = 0, PartialNum = 0,
      MinInclusions = std::numeric_limits<unsigned long int>::max();

  cout << "//-----  Start graph stats  -----//\n";
  for (unsigned int i = 0, e = AdjVect.size(); i < e; ++i) {
    const Vertex &CurVrtx = AdjVect[i];
    long int vertdgr = CurVrtx.neighborNum;
    unsigned long int inclnum = CurVrtx.coveredNum;

    if (CurVrtx.isIsolatedWithoutInclusion())
      ++IsolatedWithoutInclusionNum;
    else if (CurVrtx.isIsolatedWithInclusions()) {
      ++IsolatedWithInclusionsNum;
      if (inclnum > MaxInclusions)
        MaxInclusions = inclnum;
      if (inclnum < MinInclusions)
        MinInclusions = inclnum;
    } else {
      ++NormalNum;
      if (vertdgr > MaxDegree)
        MaxDegree = vertdgr;
      if (vertdgr < MinDegree)
        MinDegree = vertdgr;
    }
    EdgeNum += vertdgr;
    InclusionsNum += inclnum;
    PartialNum += CurVrtx.partlyCoveredVIdMap.size();
  }
  cout << "Graph has " << (vertexNum = contigGraph.vertexNum)
       << " vertexes of following types:\n" << setfill('.') << setw(32) << left
       << "Normal:" << NormalNum << "\n" << setw(32) << left
       << "IsolatedWithInclusions:" << IsolatedWithInclusionsNum << "\n"
       << setw(32) << left << "Isolated:" << IsolatedWithoutInclusionNum
       << "\n";

  if (needDetails) {
    cout << "Dumping contig graph in DOT format...\n";
    dumpContigGraph(contigGraph, contigsSets, outDirName);
    cout << "Finished with dumping contig graph\n";
  }

  cout << "\n";
  cout << setw(32) << left << "Edge num:" << (edgeNum = (EdgeNum >> 1)) << "\n";
  cout << setw(32) << left << "Inclusions num:" << (inclusionsNum = InclusionsNum) << "\n";
  cout << setw(32) << left << "Partial coverage num:" << (partialNum = (PartialNum >> 1)) << "\n";
  cout << setw(32) << left << "Min vertex degree:" << (minDegree = MinDegree) << "\n";
  cout << setw(32) << left << "Max vertex degree:" << (maxDegree = MaxDegree) << "\n";
  cout << setw(32) << left << "Min inclusions num:" << (minInclusions = MinInclusions) << "\n";
  cout << setw(32) << left << "Max inclusions num:" << (maxInclusions = MaxInclusions) << "\n";
  cout << setw(32) << left << "Average vertex degree:"
       << (averageDegree = (EdgeNum / double(NormalNum)))
       << "\n";
  cout << setw(32) << left << "Average vertex inclusion num:"
       << (averageInclusions =
              (InclusionsNum / double(IsolatedWithoutInclusionNum +
                                      IsolatedWithInclusionsNum)))
       << "\n";
  cout << setfill(' ') << "//-----  End graph stats  -----//\n";
}

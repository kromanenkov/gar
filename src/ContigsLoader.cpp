#include "include/Contigs.h"
#include "include/ContigsLoader.h"

ContigsLoader::~ContigsLoader() {
  fileNames.clear();
  fileNamesCopy.clear();
}

//check if directory name is valid and save appropriate filenames
//(LINUX compatible)
bool ContigsLoader::isAbleToLoad() {
  dirname = AH->getInDirName();

  if ((dp = opendir(dirname.c_str())) == NULL) {
    cout << "Error(" << errno << ") opening " << dirname << ".\n";
    return false;
  }

  while ((dirp = readdir(dp)) != NULL) {
    const string fileName = dirp->d_name;
    const string fullFileName = dirname + "/" + fileName;

    if (fileName[0] == '.')
      continue;
    if (stat(fullFileName.c_str(), &st) == -1)
      continue;
    const bool isDirectory = (st.st_mode & S_IFDIR) != 0;
    if (isDirectory)
      continue;
    size_t extens1 = fileName.rfind(".fa"), extens2 = fileName.rfind(".fasta");
    if ((extens1 == string::npos && extens2 == string::npos) ||
        (extens1 + 3 != fileName.size() && extens2 + 6 != fileName.size()))
        continue;

    fileNames.push_back(fullFileName);
    fileNamesCopy.insert(fileNamesCopy.begin(), fileName);
    cout << "Save " << fileName << "\n";
  }
  closedir(dp);

  if (!fileNames.size()) {
    cout << "Directory " << dirname << " doesn't contain any *.fa or *.fasta "
            "files.\n";
    return false;
  }
  filename = fileNames.back();
  fileNames.pop_back();
  cout << "Successfully load " << fileNames.size() + 1 << " files.\n";
  file.open(filename.c_str());
  assemblyTag = 0;
  return true;
}

//consequently read contigs from files in directory
Contig* ContigsLoader::getContig() {
  string line;
  Contig* C = NULL;

  while (getline(file, line)) {
    if (line[0] == '>') {
      if (seq.size() > 0) {
        (name.size())?  C = new Contig(name, seq, assemblyTag)
                       :C = new Contig(line, seq, assemblyTag);
        seq.clear();
      }
      name.clear();
      name = line;
      if (C) break;
    }
    else
      seq += line;
  }
  if (C) return C;
  if (seq.size() && name.size())
    C = new Contig(name, seq, assemblyTag);
  file.close();
  seq.clear();
  name.clear();
  if (fileNames.size()) {
    filename = fileNames.back();
    fileNames.pop_back();
    file.open(filename.c_str());
    ++assemblyTag;
    return (C)? C : getContig();
  }
  //contigs from all files are loaded
  return (C)? C: new Contig();
}

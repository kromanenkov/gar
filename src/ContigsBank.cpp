#include "include/ContigsBank.h"

ContigsBank::~ContigsBank() {
  for (int i = 0, e = contigsSets.size(); i < e; ++i)
    contigsSets[i].clear();
  contigsSets.clear();
}

// save distinct contig
void ContigsBank::addContig(const Contig& C) {
  int CurTag = C.getTag();
  for (int i = tagsNum; i < CurTag + 1; ++i)
    contigsSets.push_back(vector<Contig> ());
  tagsNum = std::max(CurTag + 1, tagsNum);
  contigsSets[CurTag].push_back(C);
}

// read contigs from files in directory
bool ContigsBank::fillContigsSets() {
  if (!checkOutputDir() || !CLoader.isAbleToLoad())
    return false;
  cout << "Ready to load contigs\n";
  Contig* C;
  for (;;) {
    C = CLoader.getContig();
    if (!C->isValid())
      break;
    addContig(*C);
    delete C;
  }
  cout << "Contigs from " << tagsNum << " files are loaded\n";
  assert(tagsNum > 1 && "Refinement process requires at least 2 assemblies\n");
  //To keep it simple, count sets' sizes after contigs loading
  setsSizes.resize(tagsNum);
  for (int i = 0; i < tagsNum; ++i)
    setsSizes[i] = contigsSets[i].size();

  return true;
}

// Write contigs to files
void ContigsBank::writeContigsSets(int numToPrint,
                                   bool MultipleFiles) const {
  string outFilePred = CLoader.getOutDirName();

  if (!numToPrint || !tagsNum) return;

  ofstream *OutputFile = NULL;
  std::string *OutputName= NULL;

  if (!MultipleFiles) {
    OutputName = new std::string(outFilePred + OutputFilename);
    OutputFile = new ofstream(OutputName->c_str());
  }

  for (int SetIndex = 0, SetIndexEnd = contigsSets.size();
       SetIndex < SetIndexEnd; ++SetIndex) {
    // Skip empty sets
    if (!contigsSets[SetIndex].size())
      continue;

    stringstream ss;
    ss << SetIndex;

    if (MultipleFiles) { // New file for each SetIndex.
      if (OutputFile) { // Reset OutputFile object.
        OutputFile->close();
        delete OutputName;
        delete OutputFile;
      }

      OutputName = new std::string(outFilePred + "gar" + ss.str() + ".fa");
      OutputFile = new ofstream(OutputName->c_str());
    }

    if (!OutputFile->is_open()) {
      cout << "Can't write to file " << OutputName << "\n";
    }

    for (int i = 0, SetSize = contigsSets[SetIndex].size();
         i < numToPrint && i < SetSize; ++i) {
      *OutputFile << contigsSets[SetIndex][i].getName() << "\n"
                  << contigsSets[SetIndex][i].getRefSeq() << "\n";
    }
  }

  if (OutputFile) {
    OutputFile->close();
    delete OutputFile;
  }
  if (OutputName)
    delete OutputName;

  cout << "Finish with contigs writing.\n";
}

bool ContigsBank::checkOutputDir() const {
  DIR *OutDir = opendir(CLoader.getOutDirName().c_str());
  if (OutDir) {
    closedir(OutDir);
    return true;
  } else if (errno == ENOENT) {
    cout << "Directory " << CLoader.getOutDirName() << " doesn't exist. Please "
         << "create it manually.\n";
    return false;
  } else {
    cout << "Can't open directory " << CLoader.getOutDirName() << " for unknown"
         << "reason. Please check permissions.\n";
    return false;
  }
}
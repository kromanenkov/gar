#include "include/ContigStitcher.h"

// FIXME: This function exists for debug purposes
void dumpVertexesInCluster(const set<long int>& cluster) {
  cout << "cluster { ";
  for (set<long int>::const_iterator I = cluster.begin(), E = cluster.end();
      I != E; ++I)
    cout << *I << "; ";
  cout << "}\n";
}

extern void dumpNeighbors(const AdjMap &AM);
extern void dumpLabels(const LabelsMap& LM);

template <typename T>
const Vertex* ContigStitcher::getMaxLenVertex(T StartIter, T EndIter) const {
  if (StartIter == EndIter)
    return NULL;

  const Vertex *MaxLenVertex = NULL;
  const Contig *MaxLenContig = NULL;

  for (; StartIter != EndIter; ++StartIter) {
    if (cGraph.vertexes[*StartIter].isMerged)
      continue;
    const Vertex *TmpVertex = &cGraph.vertexes[*StartIter];
    const Contig *TmpContig = &vertex2Contig(*TmpVertex);

    if (!MaxLenContig || (TmpContig->getLen() > MaxLenContig->getLen())) {
      MaxLenVertex = TmpVertex;
      MaxLenContig = TmpContig;
    }
  }

  return MaxLenVertex;
}

template <typename T>
Vertex* ContigStitcher::getMaxLenVertex(T StartIter, T EndIter) {
  if (StartIter == EndIter)
    return NULL;

  Vertex *MaxLenVertex = NULL;
  Contig *MaxLenContig = NULL;

  for (; StartIter != EndIter; ++StartIter) {
    if (cGraph.vertexes[*StartIter].isMerged)
      continue;
    Vertex *TmpVertex = &cGraph.vertexes[*StartIter];
    Contig *TmpContig = &vertex2Contig(*TmpVertex);

    if (!MaxLenContig || (TmpContig->getLen() > MaxLenContig->getLen())) {
      MaxLenVertex = TmpVertex;
      MaxLenContig = TmpContig;
    }
  }

  return MaxLenVertex;
}

template <typename T, typename U>
ContigStitcher::ConstNghbrIter ContigStitcher::selectBestExtension(
    const T &cont, const U &selector, const Vertex &v) const {
  // Count number of Neighbours for debug purposes
  long unsigned NghbrNum = v.adjMap.size();
  typename U::ScoreType maxval = selector.getLowerBound(), tmpval;
  ConstNghbrIter CNI = v.adjMap.end(), TempCNI;

  for (typename T::const_iterator I = cont.begin(), E = cont.end();
       I != E; ++I) {
//    if (cGraph.vertexes[*I].isMerged)
//      continue;
    TempCNI = v.adjMap.find(*I);
    if (TempCNI == v.adjMap.end())
      continue;

    tmpval = selector.getScore(TempCNI->second);
    if (tmpval > maxval) {
      maxval = tmpval;
      CNI = TempCNI;
    }
  }

  // Check that all elements from cont are neighbours of v
  assert(v.adjMap.size() == NghbrNum);
  return CNI;
}

inline bool ContigStitcher::isLeftExtension(const Vertex &v,
                                            ConstNghbrIter CNI) const {
  const Vertex &NghbrV = cGraph.vertexes[CNI->first];

  return (((CNI->second->seq1end == vertex2Contig(NghbrV).getLen() - 1) &&
           (NghbrV.setId > v.setId)) ||
          ((CNI->second->seq2end == vertex2Contig(NghbrV).getLen() - 1) &&
           (v.setId > NghbrV.setId)));
}

inline bool ContigStitcher::isRightExtension(const Vertex &v,
                                             ConstNghbrIter CNI) const {
  const Vertex &NghbrV = cGraph.vertexes[CNI->first];

  return (((CNI->second->seq1end == vertex2Contig(v).getLen() - 1) &&
           (v.setId > NghbrV.setId)) ||
          ((CNI->second->seq2end == vertex2Contig(v).getLen() - 1) &&
           (NghbrV.setId > v.setId)));
}

ContigStitcher::VIdFastContainer ContigStitcher::getPotentialDirectedExtension(
    const Vertex& v, const ClusterTy& cluster,
    const Direction& Direction) const {
  VIdFastContainer ResultVIds;
  ResultVIds.set_empty_key(-1);
  const ExtArrayTy *ExtArray;

  if (Direction.isInbound())
    ExtArray = &v.adjMap.InboundNeighbors;
  else // Outbound direction.
    ExtArray = &v.adjMap.OutboundNeighbors;

  // Neighbour vertex lies in another cluster or it's already merged or it's
  // covered by unmerged root node.
  for (ExtArrayTy::const_iterator CI = ExtArray->begin(), CE = ExtArray->end();
       CI != CE; ++CI) {
    if (!cluster.count(*CI) || cGraph.vertexes[*CI].isMerged ||
        isCoveredByRootVertex(*CI))
      continue;

    ResultVIds.insert(*CI);
  }

  return ResultVIds;
}

ContigStitcher::VIdOrderedContainer ContigStitcher::extendInDirection(
    const Vertex* seed, VIdFastContainer* mergeset, Contig* C,
    const ClusterTy& cluster, const Direction& InitDir,
    Direction &FinalDir) const {
  const Vertex *CurVertex = seed;

  Direction CurrentDir(InitDir);

  VIdOrderedContainer MergedVertexes;
  VIdFastContainer PotentialExtension =
      getPotentialDirectedExtension(*CurVertex, cluster, CurrentDir);

  while (!isSubset<VIdFastContainer>(PotentialExtension, *mergeset)) {
    ConstNghbrIter ExtensionIter = selectBestExtension(PotentialExtension,
        weights::ThresholdScoreWeight<>(), *CurVertex);

    if (ExtensionIter == CurVertex->adjMap.end())
      break;

    long int mergedVertId = ExtensionIter->first;
    // getPotentialDirectedExtension returns only unmerged vertexes
    const Vertex *VertexToMerge = &cGraph.vertexes[mergedVertId];
    VertexToMerge->markAsMerged(cGraph.vertexes);
    MergedVertexes.push_back(mergedVertId);
    mergeset->insert(mergedVertId);

    const Contig &ContigToMerge = vertex2Contig(*VertexToMerge);
    const EdgeInfo *EI = ExtensionIter->second;

    if (EdgeInfo::strandIsChanged(EI->ST))
          CurrentDir.flip();

    if (CurVertex->setId > VertexToMerge->setId) { // VertexToMerge -> seq2.
      if (InitDir.isInbound()) {
        if (CurrentDir == InitDir) {
          C->extendLeft(ContigToMerge, EI->seq2start, EI->seq2end,
                        EI->seq1start, EI->seq1end);
        } else {
          C->extendLeft(ContigToMerge.getRC(),
                        ContigToMerge.getLen() - EI->seq2end - 1,
                        ContigToMerge.getLen() - 1, EI->seq1start, EI->seq1end);
        }
      } else { // Outbound direction.
        if (CurrentDir == InitDir) {
          C->extendRight(ContigToMerge, EI->seq1start, EI->seq1end,
                         EI->seq2start, EI->seq2end);
        } else {
          C->extendRight(ContigToMerge.getRC(), EI->seq1start, EI->seq1end, 0,
                         ContigToMerge.getLen() - EI->seq2start - 1);
        }
      }
    } else { // VertexToMerge -> seq1.
      if (InitDir.isInbound()) {
        if (CurrentDir == InitDir) {
          C->extendLeft(ContigToMerge, EI->seq1start, EI->seq1end,
                        EI->seq2start, EI->seq2end);
        } else {
          C->extendLeft(ContigToMerge.getRC(),
                        ContigToMerge.getLen() - EI->seq1end - 1,
                        ContigToMerge.getLen() - 1, EI->seq2start,
                        EI->seq2end);
        }
      } else { // Outbound direction.
        if (CurrentDir == InitDir) {
          C->extendRight(ContigToMerge, EI->seq2start, EI->seq2end,
                         EI->seq1start, EI->seq1end);
        } else {
          C->extendRight(ContigToMerge.getRC(), EI->seq2start, EI->seq2end, 0,
                         ContigToMerge.getLen() - EI->seq1start - 1);
        }
      }
    }

    CurVertex = VertexToMerge;
    PotentialExtension =
        getPotentialDirectedExtension(*CurVertex, cluster, CurrentDir);
  }

  FinalDir = CurrentDir;
  return MergedVertexes;
}

ContigStitcher::VIdFastContainer ContigStitcher::getTrimCandidatesFromDirection(
    const Vertex& v, const ClusterTy& cluster,
    const Direction& Direction) const {
  VIdFastContainer TrimVariants;
  TrimVariants.set_empty_key(-1);
  const ExtArrayTy *ExtArray;
  const VIdFastContainer *Bound;

  if (Direction.isInbound()) {
    ExtArray = &v.adjMap.InboundNeighbors;
    Bound = &outboundEnds;
  }
  else {
    ExtArray = &v.adjMap.OutboundNeighbors;
    Bound = &inboundEnds;
  }

  for (ExtArrayTy::const_iterator CI = ExtArray->begin(), CE = ExtArray->end();
       CI != CE; ++CI) {
    // Neighbour vertex is not a boundary sequence in cluster or it's not
    // already merged (WTF, why?!) or it lays in same cluster.
    if (!Bound->count(*CI) || !cGraph.vertexes[*CI].isMerged ||
        cluster.count(*CI))
      continue;

    TrimVariants.insert(*CI);
  }

  return TrimVariants;
}

void ContigStitcher::trimContig(Contig *C,
                                const VIdOrderedContainer &InboundExt,
                                const VIdOrderedContainer &OutboundExt,
                                const ClusterTy &cluster,
                                const Direction &InboundDir,
                                const Direction &OutboundDir) const {
  long InboundEnd = InboundExt.back(), OutboundEnd = OutboundExt.back();
  const Vertex &InboundExtVrtx = cGraph.vertexes[InboundEnd],
               &OutboundExtVrtx = cGraph.vertexes[OutboundEnd];
  ConstNghbrIter InboundEndIter = InboundExtVrtx.adjMap.end(),
                 OutboundEndIter = OutboundExtVrtx.adjMap.end();

  VIdFastContainer TrimCandidatesFromInbound =
      getTrimCandidatesFromDirection(InboundExtVrtx, cluster, InboundDir),
                   TrimCandidatesFromOutbound =
      getTrimCandidatesFromDirection(OutboundExtVrtx, cluster, OutboundDir);

  ConstNghbrIter InboundCNI = selectBestExtension(
      TrimCandidatesFromInbound, weights::AlignLengthWeight<>(),
      InboundExtVrtx);
  if (InboundCNI != InboundEndIter) {
    const EdgeInfo *EI = InboundCNI->second;
    const Contig &InboundExtContig = vertex2Contig(InboundExtVrtx);
    const long LExtLen = InboundExtContig.getLen();

    if (InboundExtVrtx.setId > cGraph.vertexes[InboundCNI->first].setId) {
      if (InboundDir.isInbound()) {
        C->trimFromLeft(EI->seq1start, EI->seq1end);
      } else {
        C->trimFromLeft(0, LExtLen - EI->seq1end - 1);
      }
    } else { // InboundExtContig -> seq2
      if (InboundDir.isInbound()) {
        C->trimFromLeft(EI->seq2start, EI->seq2end);
      } else {
        C->trimFromLeft(0, LExtLen - EI->seq2end - 1);
      }
    }
  }

  ConstNghbrIter OutboundCNI = selectBestExtension(
      TrimCandidatesFromOutbound, weights::AlignLengthWeight<>(),
      OutboundExtVrtx);
  if (OutboundCNI != OutboundEndIter) {
    const EdgeInfo *EI = OutboundCNI->second;
    const Contig &OutboundExtContig = vertex2Contig(OutboundExtVrtx);
    const long StitchLen = C->getLen(), RExtLen = OutboundExtContig.getLen();

    if (OutboundExtVrtx.setId > cGraph.vertexes[OutboundCNI->first].setId) {
      if (OutboundDir.isOutbound()) {
        // Avoid contig "overtrimming".
        int StartBound = std::max(0L, StitchLen - RExtLen + EI->seq1start);
        C->trimFromRight(StartBound, StitchLen - RExtLen + EI->seq1end);
      } else {
        // Avoid contig "overtrimming".
        int StartBound = std::max(0L, StitchLen - EI->seq1end - 1);
        C->trimFromRight(StartBound, StitchLen - 1);
      }
    } else { // OutboundExtContig -> seq2
      if (OutboundDir.isOutbound()) {
        // Avoid contig "overtrimming".
        int StartBound = std::max(0L, StitchLen - RExtLen + EI->seq2start);
        C->trimFromRight(StartBound, StitchLen - RExtLen + EI->seq2end);
      } else {
        // Avoid contig "overtrimming".
        int StartBound = std::max(0L, StitchLen - EI->seq2end - 1);
        C->trimFromRight(StartBound, StitchLen - 1);
      }
    }
  }

  inboundEnds.insert(InboundEnd);
  outboundEnds.insert(OutboundEnd);
}

ContigStitcher::ConstNghbrIter ContigStitcher::getNextOverlappedVertex(
    const Vertex &V, const set<long int> &cluster) const {
  double OverlapScore = SCORE_THRESHOLD - 1.;
  ConstNghbrIter CNI = V.adjMap.end(), TempCNI;

  for (set<long int>::const_iterator I = cluster.begin(), E = cluster.end();
       I != E; ++I) {
    TempCNI = V.adjMap.find(*I);
    // Neighbour vertex lies in another cluster or we find ourselfs
    if (TempCNI == V.adjMap.end() || &cGraph.vertexes[TempCNI->first] == &V)
      continue;

    if (TempCNI->second->bitScore > OverlapScore) {
      CNI = TempCNI;
      OverlapScore = TempCNI->second->bitScore;
    }
  }

  return CNI;
}

Contig* ContigStitcher::
    stitchContigsPairInCluster(const set<long int>& cluster) const {
  const Vertex *MaxLenVertex = getMaxLenVertex<set<long int>::const_iterator>(
      cluster.begin(), cluster.end());
  if (!MaxLenVertex)
    return NULL;

  const Contig &MaxLenContig = vertex2Contig(*MaxLenVertex);
  ConstNghbrIter MaxLenNghbr =
      getNextOverlappedVertex(*MaxLenVertex, cluster);
  Contig *C = NULL;

  do {
    if (MaxLenNghbr != MaxLenVertex->adjMap.end()) {
      const EdgeInfo *EI = MaxLenNghbr->second;

      if (EI->eValue < DBL_MIN) {
        const Vertex *MaxLenNghbrVertex = &cGraph.vertexes[MaxLenNghbr->first];
        int MaxLenContigSet = MaxLenVertex->setId,
            MaxLenNghbrSet = MaxLenNghbrVertex->setId;

        if (MaxLenNghbrSet > MaxLenContigSet)
          std::swap(MaxLenNghbrVertex, MaxLenVertex);

        C = new Contig(vertex2Contig(*MaxLenVertex),
                       vertex2Contig(*MaxLenNghbrVertex), EI->seq1start,
                       EI->seq1end, EI->seq2start, EI->seq2end, Tag);

        break;
      }
    }
    C = new Contig(MaxLenContig);
  } while (0);

  C->setTag(Tag);
  return C;
}

Contig* ContigStitcher::stitchContigsInCluster(
    long int SeedId, const ClusterTy &cluster) {
  const Vertex *SeedVertex = &cGraph.vertexes[SeedId];
  assert(!SeedVertex->isMerged);

  const Contig &SeedContig = vertex2Contig(*SeedVertex);
  Contig *C = new Contig(SeedContig);

  VIdFastContainer mergeset;
  mergeset.set_empty_key(-1);
  mergeset.insert(SeedId);
  SeedVertex->markAsMerged(cGraph.vertexes);

  Direction In(Direction::Inbound), Out(Direction::Outbound),
            FinalIn(Direction::Inbound), FinalOut(Direction::Outbound);

  VIdOrderedContainer InboundExt =
      extendInDirection(SeedVertex, &mergeset, C, cluster, In, FinalIn);
  if (!InboundExt.size())
    InboundExt.push_back(SeedId);

  VIdOrderedContainer OutboundExt =
      extendInDirection(SeedVertex, &mergeset, C, cluster, Out, FinalOut);
  if (!OutboundExt.size())
    OutboundExt.push_back(SeedId);

  trimContig(C, InboundExt, OutboundExt, cluster, FinalIn, FinalOut);
  C->setTag(Tag);
  return C;
}

unsigned long int ContigStitcher::getInclusionsNumInCluster(
    const ClusterTy &cluster) const {
  unsigned long int num = 0;

  for (ClusterTy::const_iterator I = cluster.begin(), E = cluster.end();
       I != E; ++I) {
    num += cGraph.vertexes[*I].coveredNum;
  }
  return num;
}

bool ContigStitcher::InclusionComparator::operator () (
    const long int &clustid1, const long int &clustid2) const {
  // Local typedef to simplify iterations.
  typedef CoverageManager::ClusterCoverageMapTy ClusterCoverageMapTy;
  const ClusterCoverageMapTy &CCM = CS->CM.getClusterCoverageMap();

  ClusterCoverageMapTy::const_iterator I1 = CCM.find(clustid1),
                                       I2 = CCM.find(clustid2);
  assert(I1 != CCM.end() && I2 != CCM.end());

  return I1->second.CoverageLen > I2->second.CoverageLen;
}

void ContigStitcher::fillClustersWorklist() const {
  for (ClustersMapTy::const_iterator I = cMap.begin(), E = cMap.end();
       I != E; ++I) {
//    if (I->second.size() < (cBank.getSetsSizes().size() >> 1))
//      continue;
//    if (I->second.size() < 4)
//      continue;

    clustersWorklist.insert(
        std::upper_bound(clustersWorklist.begin(), clustersWorklist.end(),
            I->first, InclusionComparator(this)),
        I->first);
  }
}

bool ContigStitcher::isCoveredByRootVertex(long int vrtxid) const {
  const Vertex *V = &cGraph.vertexes[vrtxid];

  for (AdjMap::const_iterator I = V->parentVIdMap.begin(),
                              E = V->parentVIdMap.end(); I != E; ++I)
    if (RS.count(I->first))
      return true;

  return false;
}

void ContigStitcher::stitchContigsInAllClusters() {
  fillClustersWorklist();

  // Get roots set for all clusters.
  RS = CM.getRoots();
  const CoverageManager::ClusterCoverageMapTy &CCM = CM.getClusterCoverageMap();
  // Local typedefs to simplify iterating.
  typedef CoverageManager::ClusterCoverageMapTy::const_iterator CCMIter;
  typedef CoverageManager::ClusterCoverageInfo::ClusterRootsTy ClusterRoootsTy;

  // Contigs stitching in cluster starts from root vertexes.
  for (ClusterWLTy::const_iterator I = clustersWorklist.begin(),
                                   E = clustersWorklist.end(); I != E; ++I) {
    const ClustersMapTy::const_iterator CMI = cMap.find(*I);
    CCMIter CoverageIter = CCM.find(*I);
    if (CMI == cMap.end() || CoverageIter == CCM.end())
      continue;

    const ClusterRoootsTy &CR = CoverageIter->second.getClusterRoots();
    for (ClusterRoootsTy::const_iterator II = CR.begin(), EE = CR.end();
         II != EE; ++II) {
      Contig *StitchedContig;

      // Root vertex is already covered by another path in cluster.
      if (cGraph.vertexes[*II].isMerged) {
        RS.erase(*II);
        continue;
      }
      StitchedContig = stitchContigsInCluster(*II, CMI->second);
      if (StitchedContig->getLen() > 500)
        contigsToWrite.addContig(*StitchedContig);
      delete StitchedContig;
      RS.erase(*II);
    }
  }
}

void ContigStitcher::addIsolatedRootVertexes(unsigned int threshold) {
  for (CoverageManager::RootsSetTy::const_iterator I = RS.begin(), E = RS.end();
       I != E; ++I) {
    // Vertex alias.
    const Vertex &V = cGraph.vertexes[*I];

    if (V.isMerged)
      continue;
    assert(!V.hasClusterLabel());

    Contig C(vertex2Contig(cGraph.vertexes[*I]));
    C.setTag(Tag);
    if (C.getLen() <= 500)
      continue;

    bool NeedToAdd = V.coveredNum > threshold;
    for (AdjMap::const_iterator CI = V.coveredVIdMap.begin(),
                                CE = V.coveredVIdMap.end();
         !NeedToAdd && CI != CE; ++CI) {
      if (cGraph.vertexes[CI->first].hasClusterLabel())
        NeedToAdd = true;
    }

    if (!NeedToAdd)
      continue;
    V.markAsMerged(cGraph.vertexes);
    contigsToWrite.addContig(C);
  }
}

void ContigStitcher::writeResults(const std::string &outFilePred) const {
  cout << "Writing contigs to the " << outFilePred << OutputFilename
       << " file...\n";
  contigsToWrite.writeContigsSets(50000);
}

int main(int argc, char **argv) {
  ArgumentHolder *ArgHolder = new ArgumentHolder(argc, argv);
  if (ArgHolder->getShowHelp()) { // Quit after help printing.
    delete ArgHolder;
    return 0;
  }

  ContigsBank CBank(ArgHolder);
  if (!CBank.fillContigsSets())
    return 1;

  StatisticsHolder *StatsHolder =
      new StatisticsHolder(ArgHolder->getShowStat());
  StatsHolder->setSetsSizes(CBank.getSetsSizes());
  StatsHolder->setSetsNames(CBank.getSetsNames());
  StatsHolder->printInputStats(CBank.getContigsSets());

  GraphBuilder *GBuilder =
      new GraphBuilder(CBank.getSetsSizes(), ArgHolder->getInclThr());

  if (ArgHolder->getLoad()) {
    int ErrCode;

    GBuilder->allocScoreMatrix();
    ErrCode =
        GBuilder->loadScoreMatrix(ArgHolder->getScoresLoadDirName() + "set");
    if (ErrCode == -1)
      return 1;
  }
  else
    GBuilder->fillScoreMatrix(CBank.getContigsSets(), CBank.getIndirName(),
                              CBank.getSetsNames(),
                              StatsHolder->getAvInputLens());
  StatsHolder->printScoreMatrixStats(GBuilder->getScoreMatrix().matrix);
  if (ArgHolder->getDump())
    GBuilder->dumpScoreMatrix(ArgHolder->getScoresDumpDirName() + "set");

  if (GBuilder->buildGraph(CBank.getContigsSets()) != 1)
    return 1;
  StatsHolder->printContigGraphStats(GBuilder->getContigGraph(),
                                     CBank.getContigsSets(),
                                     ArgHolder->getOutDirName());

  GraphSplitterBase<GraphSplittingInst> *GSB =
      new GraphSplittingInst(GBuilder->getContigGraph(), 0.1, 1.5, 0.9, 5);
  GSB->runClustering();

  ContigStitcher *CStitcher = new ContigStitcher(
      CBank, GBuilder->getContigGraph(), GSB->getClusterLabels(), ArgHolder);
  CStitcher->stitchContigsInAllClusters();
  CStitcher->addIsolatedRootVertexes(CBank.getContigsSets().size() >> 1);
  CStitcher->writeResults(ArgHolder->getOutDirName());

  delete CStitcher;
  delete GSB;
  delete GBuilder;
  delete StatsHolder;
  delete ArgHolder;

  return 0;
}

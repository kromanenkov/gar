#include "include/CoverageManager.h"

bool CoverageManager::RootNodesComparator::operator () (
    const long int& id1, const long int& id2) const {
  int set1id = CM->CG.vertexes[id1].setId,
      contig1id = CM->CG.vertexes[id1].contigId,
      set2id = CM->CG.vertexes[id2].setId,
      contig2id = CM->CG.vertexes[id2].contigId;

  return CM->CB.getContigsSet(set1id)[contig1id].getLen() >
         CM->CB.getContigsSet(set2id)[contig2id].getLen();
}

CoverageManager::CoverageManager(
    const ContigsBank &CB, const ContigGraph& CG,
    const std::map<long int, ClusterTy>& ClustersMap, double PartCovThr)
    : CB(CB), CG(CG), PartialCoverageThreshold(PartCovThr) {
  cout << "Initialize CoverageManager object with partial coverage threshold="
       << PartialCoverageThreshold << ";\n";

  coveredVertexes.resize(CG.vertexNum, 0);
  roots.set_empty_key(-1);
  roots.set_deleted_key(-2);
  // Local typedef to simplify iterating.
  typedef std::map<long int, ClusterTy> ClustersMapTy;

  for (ClustersMapTy::const_iterator I = ClustersMap.begin(),
                                     E = ClustersMap.end(); I != E; ++I)
    CCM.insert(std::make_pair(I->first, ClusterCoverageInfo(I->second)));

  // Calculate different coverage statistics fo each cluster.
  countClustersCoverageStats();
  // Found root nodes of coverage tree.
  formRootSet();
  // Found and mark as merged vertexes covered by vertexes combination.
  formCoveredByCombinationSet();
}

void CoverageManager::formRootSet() {
  const std::vector<Vertex> &VertexesRef = CG.vertexes;

  for (long int i = 0, e = coveredVertexes.size(); i < e; ++i) {
    if (coveredVertexes[i])
      continue;

    // Nodes with no inclusions and parent nodes couldn't be root nodes.
    if (VertexesRef[i].coveredNum == 0 &&
        VertexesRef[i].parentVIdMap.size() == 0) {
      coveredVertexes[i] = 1;
      continue;
    }

    if (VertexesRef[i].parentVIdMap.size())
      coveredVertexes[i] = 1;

    const AdjMap &CoveredMap = VertexesRef[i].coveredVIdMap;
    for (AdjMap::const_iterator I = CoveredMap.begin(), E = CoveredMap.end();
         I != E; ++I)
      if (I->second->ET != EdgeInfo::Identical)
        coveredVertexes[I->first] = 1;
  }
  // All uncovered vertexes becomes root nodes.
  for (long int i = 0, e = coveredVertexes.size(); i < e; ++i)
    if (!coveredVertexes[i]) {
      // Alias for root vertex.
      const Vertex &RootVrtx = CG.vertexes[i];

      if (RootVrtx.hasClusterLabel()) {
        ClusterCoverageMapTy::iterator I = CCM.find(RootVrtx.clusterId);
        if (I != CCM.end()) {
          ClusterCoverageInfo::ClusterRootsTy &ClusterRoots =
              I->second.getClusterRoots();
          // Add vertex to cluster root vertexes. Sorted by length descending
          // order.
          ClusterRoots.insert(
              std::upper_bound(ClusterRoots.begin(), ClusterRoots.end(), i,
                               RootNodesComparator(this)),
              i);
          roots.insert(i);
        }
      } else { // No cluster label, isolated vertex.
        assert(RootVrtx.isIsolated());

        if (RootVrtx.isIsolatedWithInclusions())
          roots.insert(i);
      }
    }
}

void CoverageManager::formCoveredByCombinationSet() {
  const std::vector<Vertex> &VertexesRef = CG.vertexes;

  for (long unsigned int i = 0, e = VertexesRef.size(); i < e; ++i) {
    // Skip vertexes that are covered by one of the root node.
    if (coveredVertexes[i])
      continue;

    // Aliases.
    const Vertex &V = VertexesRef[i];
    const AdjMap &PCM = V.partlyCoveredVIdMap;
    int VSetId = V.setId;
    // Vertex's contig length.
    long len = CB.getContigsSet(V.setId)[V.contigId].getLen();
    // Covered bases in vertex contig.
    SeqCoveredBases Seq;
    Seq.resize(len, 0);
    unsigned int CoveredBases = 0;

    // Accumulate partially covered sequence ranges.
    for (AdjMap::const_iterator I = PCM.begin(), E = PCM.end(); I != E; ++I) {
      int NghbrSetId = VertexesRef[I->first].setId;
      const EdgeInfo *EI = I->second;

      if (VSetId > NghbrSetId)
        std::fill(&Seq[EI->seq1start], &Seq[EI->seq1end], 1);
      else
        std::fill(&Seq[EI->seq2start], &Seq[EI->seq2end], 1);
    }
    for (long ii = 0; ii < len; ++ii)
      if (Seq[ii])
        ++CoveredBases;

    if (CoveredBases >= (unsigned int) (len * PartialCoverageThreshold)) {
      V.isMerged = true;
      CoveredByCombination.push_back(i);
    }
  }
}

void CoverageManager::countClustersCoverageStats() {
  for (ClusterCoverageMapTy::iterator I = CCM.begin(), E = CCM.end(); I != E;
       ++I) {
    long double CoveredLen = 0.0;
    unsigned long CoverageNum = 0;
    const ClusterTy &Cluster = I->second.cluster;
    // Cluster vertexes cycle.
    for (ClusterTy::const_iterator II = Cluster.begin(), EE = Cluster.end();
         II != EE; ++II) {
      if (CG.vertexes[*II].coveredNum == 0)
        continue;
      const AdjMap &AM = CG.vertexes[*II].coveredVIdMap;
      // Vertexes covered by current vertex cycle.
      for (AdjMap::const_iterator III = AM.begin(), EEE = AM.end();
           III != EEE; ++III) {
        // Do not count coverage len of identical vertixes.
        if (III->second->ET != EdgeInfo::Identical) {
          CoveredLen += III->second->alignlength;
          ++CoverageNum;
        }
      }
    }
    I->second.CoverageLen = CoveredLen;
    I->second.CoverageNum = CoverageNum;
  }
}

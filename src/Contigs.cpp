#include "include/Contigs.h"

Contig::Contig(string name_, string seq_, int assemblyTag_) {
  name = name_;
  seq = seq_;
  len = seq.size();
  assemblyTag = assemblyTag_;
}
// Merging contigs constructor
Contig::Contig(const Contig& c1, const Contig& c2, int seq1start, int seq1end,
               int seq2start, int seq2end, int tag_) {
  name = ">Merged (" + c1.getRefName() + ")+(" + c2.getRefName() + ")";

  if (seq1start >= c1.getLen() || seq2start >= c2.getLen()) {
    cout << "1: " << seq1start << "-" << seq1end << "; Len = " << c1.getLen() << "\n";
    cout << "2: " << seq2start << "-" << seq2end << "; Len = " << c2.getLen() << "\n";
    cout << "Name1 = " << c1.getRefName() << "\n";
    cout << "Name2 = " << c2.getRefName() << "\n";
  }

  assert(seq1start < c1.getLen() && seq2start < c2.getLen() &&
         "Sequence start position is out of bound");

  const Contig *C1Ptr = &c1, *C2Ptr = &c2;

  // In such case:
  //       ----------------- c1
  //       ^seq1start
  // ----------- c2
  //       ^seq2start
  if (seq1start == 0) { // keep synced with BlastRunnerBase::isPossibleToMerge
    std::swap(C1Ptr, C2Ptr);
    std::swap(seq1start, seq2start);
    std::swap(seq2end, seq2end);
  }

  seq = C1Ptr->getRefSeq().substr(0, seq1start).
            append(C2Ptr->getRefSeq(), seq2start, string::npos);
  len = seq.size();
  assemblyTag = tag_;
}

void Contig::extendRight(const Contig& c, int seq1start, int seq1end,
                         int seq2start, int seq2end) {
  assert(seq1start < len && seq2end + 1 < c.getLen() &&
         "Sequence start position is out of bound");

  name += "->(" + c.getName().substr(1, string::npos) + ")";
  seq = seq.append(c.getRefSeq(), seq2end + 1, string::npos);

  assert((long) seq.size() > len);

  len = seq.size();
}

void Contig::extendLeft(const Contig& c, int seq1start, int seq1end,
                        int seq2start, int seq2end) {
  assert(seq1start < c.getLen() && seq2start < len &&
         "Sequence start position is out of bound");

  name = ">(" + c.getName().substr(1, string::npos) + ")<-(" +
         name.substr(1, string::npos) + ")";
  seq = c.getRefSeq().substr(0, seq1start).append(seq);

  assert((long) seq.size() > len);

  len = seq.size();
}

void Contig::trimFromLeft(int seq1start, int seq1end) {
  assert(seq1end + 1 < len && "Left trim seqend position is out of bound");

  //seq.erase(seq1start, seq1end - seq1start + 1);
  seq = seq.substr(seq1end + 1, string::npos);

  assert(len > (long) seq.size());

  len = seq.size();
}

void Contig::trimFromRight(int seq1start, int seq1end) {
  assert(seq1start < len && "Right trim seqstart position is out of bound");

  seq.erase(seq1start, string::npos);

  assert(len > (long) seq.size());

  len = seq.size();
}

Contig::~Contig() {
  name.clear();
  seq.clear();
}

Contig Contig::getRC() const {
  Contig RC(*this);

  ncbi::revcmp(&*RC.getBareSeq().begin(), 0, len,
               ncbi::CIupacnaCmp::GetTable());
  RC.setName(RC.getRefName() + "_RK");

  return RC;
}

void Contig::dump() const {
  cout << "--- Contig dump start ---\n";
  cout << ">" << name << "\n" << seq << "\nLen = " << len << ", Tag = " <<
          assemblyTag << "\n";
  cout << "--- Contig dump end -----\n";
}

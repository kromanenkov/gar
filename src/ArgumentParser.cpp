#include <iostream>

#include "include/ArgumentParser.h"

ArgumentHolder::ArgumentHolder(int argc, char** argv):
    showStat(false), dump(false), load(false), inDirName("."),
    outDirName("./Out/"), scoresDumpDirName("./tmp/"),
    scoresLoadDirName("./tmp/"), showHelp(false), inclThr(0.9),
    partCovThr(0.999) {
  argc -= (argc>0); argv += (argc>0); // skip program name argv[0] if present

  //TODO: move this out from constructor
  option::Descriptor usage[] = {
    {UNKNOWN, 0, "", "", option::Arg::None,
     "GAR - Genome Assembly Refinement tool for reconciliation contigs sets "
     "obtained from different genome assemblers.\n\n"
     "USAGE: gar --indir=<indirpath> [options]\n\nGAR takes as input all FASTA "
     "files in <indirpath> and writes result to the gar.fa in output directory"
     " (default is ./Out/).\n\nOptions:"},
    {INDIR, 0, "i", "indir", option::Arg::Optional,
     "  --indir=<indirpath> \tPath to directory wih input fasta files with "
     "assemblies. Default path is './'."},
    {OUTDIR, 0, "o", "outdir", option::Arg::Optional,
     "  --outdir=<outdirpath> \tPath to directory with results. The directory "
     "should exist. Default path is './Out/'."},
    {INCL_THR, 0, "", "incl_threshold", option::Arg::Optional,
     "  --incl_threshold=<threshold> \tInclusion threshold value. If "
     "more than (incl_threshold*100)% symbols of one sequence are contained"
     "in another, it is considered that the latter includes the former. Default "
     "is 0.9."},
    {PART_COV_THR, 0, "", "partcov_threshold", option::Arg::Optional,
     "  --partcov_threshold=<threshold> \tPartial coverage threshold. If "
     "more than (partcov_threshold*100)% symbols of the sequence are covered "
     "by the combination of other sequences, it is eliminated from the final "
     "set. Default is 0.999."},
    {STATS, 0, "s", "stat", option::Arg::None,
     "  --stat, -s  \tShows extended stats."},
    {DUMP, 0, "d", "dump", option::Arg::Optional,
     "  --dump=<scoresmatrpath>, -d \tDump matrix with align scores. "
     "Default path is './tmp/'."},
    {LOAD, 0, "l", "load", option::Arg::Optional,
     "  --load=<scoresmatrpath>, -l \tLoad matrix with align scores. "
     "Default path is './tmp/'."},
    {HELP, 0, "h", "help", option::Arg::None, "  --help  \tPrint usage."},
    {UNKNOWN, 0, "", "", option::Arg::None,
      "\nExamples:\n"
      "  gar --indir=/path/to/data --load=/path/to/scorematr \n"
      "  gar --indir=/path/to/data -s --outdir=/path/to/outputdir \n"
      "  gar --indir=/path/to/data --incl_threshold=0.9"},
    {0, 0, 0, 0, 0, 0}};
  option::Stats stats(usage, argc, argv);
  option::Option options[stats.options_max], buffer[stats.buffer_max];
  option::Parser parse(usage, argc, argv, options, buffer);

  if (options[STATS])
    showStat = true;
  if (options[DUMP]) {
    dump = true;
    setScoresDumpDirName(options[DUMP].arg);
  }
  if (options[LOAD]) {
    load = true;
    setScoresLoadDirName(options[LOAD].arg);
  }
  if (options[INDIR])
    setInDirName(options[INDIR].arg);
  if (options[OUTDIR])
    setOutDirName(options[OUTDIR].arg);
  if (options[HELP] || argc == 0) {
    option::printUsage(std::cout, usage);
    showHelp = true;
  }

  // Move #define constants to program arguments.
  if (options[INCL_THR])
    inclThr = std::stof(options[INCL_THR].arg);
  if (options[PART_COV_THR])
    partCovThr = std::stod(options[PART_COV_THR].arg);
}

void ArgumentHolder::setInDirName(const char *s) {
  if (s && string(s) != "")
    inDirName = string(s);
  if (*inDirName.rbegin() != '/')
    inDirName += '/';
}

void ArgumentHolder::setOutDirName(const char *s) {
  if (s && string(s) != "")
    outDirName = string(s);
  if (*outDirName.rbegin() != '/')
    outDirName += '/';
}

void ArgumentHolder::setScoresDumpDirName(const char *s) {
  if (s && string(s) != "")
    scoresDumpDirName = string(s);
  if (*scoresDumpDirName.rbegin() != '/')
    scoresDumpDirName += '/';
}

void ArgumentHolder::setScoresLoadDirName(const char *s) {
  if (s && string(s) != "")
    scoresLoadDirName = string(s);
  if (*scoresLoadDirName.rbegin() != '/')
    scoresLoadDirName += '/';
}

#ifndef GRAPHHELPERTYPES_H_
#define	GRAPHHELPERTYPES_H_

#include <iostream>
#include <omp.h>

#include <sparsehash/sparse_hash_map>
#include <sparsehash/dense_hash_map>
#include <sparsehash/dense_hash_set>

#define SCORE_THRESHOLD 1.0
#define CLUSTERING_EPS 0.0001

using google::sparse_hash_map;
using google::dense_hash_map;
using google::dense_hash_set;

// Structure to hold information about pair of aligned contigs.
struct EdgeInfo {
  enum EdgeType {
    // Ordinary overlapping of two contigs.
    Normal = 0,
    // Contig from the first vertex contains the second contig's sequence.
    FirstCovers = 1,
    // Contig from the second vertex contains the first contig's sequence.
    SecondCovers = 2,
    // Contigs are identical.
    Identical = 3,
    // Contigs have identical contiguous part.
    PartialCoverage = 4
  };

  enum StrandType {
    // If contigs' sequences lie on the same 'plus' strand.
    PlusPlus = 0,
    // If contigs' sequences lie on different 'plus/minus' strands.
    PlusMinus = 1,
    // If contigs' sequences lie on different 'minus/plus' strands.
    MinusPlus = 2,
    // If contigs' sequences lie on the same 'minus' strand. Weird case.
    MinusMinus = 3
  };

  static bool strandTypePlusStart(StrandType ST) {
    return ST == PlusPlus || ST == PlusMinus;
  }
  static bool strandTypeMinusStart(StrandType ST) {
    return ST == MinusMinus || ST == MinusPlus;
  }
  static bool strandTypePlusEnd(StrandType ST) {
    return ST == PlusPlus || ST == MinusPlus;
  }
  static bool strandTypeMinusEnd(StrandType ST) {
    return ST == MinusMinus || ST == PlusMinus;
  }
  // Check if query and db sequences lay on different strands.
  static bool strandIsChanged(StrandType ST) {
    return ST == PlusMinus || ST == MinusPlus;
  }

  // Flip query and db sequences strand order.
  static StrandType flip(StrandType ST) {
    switch (ST) {
      case PlusPlus:
        return ST;
      case PlusMinus:
        return MinusPlus;
      case MinusPlus:
        return PlusMinus;
      case MinusMinus:
        return ST;
    }
    assert(0 && "Unreachable program point\n");
  }

  EdgeType ET;
  StrandType ST;
  double bitScore;
  int numIdent;
  double eValue;
  unsigned int seq1start, seq1end, seq2start, seq2end;
  unsigned int alignlength;

  EdgeInfo(): ET(Normal), ST(PlusPlus), bitScore(0), numIdent(-1), eValue(0),
      seq1start(-1), seq1end(-1), seq2start(-1), seq2end(-1), alignlength(-1) {}

  void dump() const {
    std::cout << "/////   Elem   /////\ntype = " << ET << "\nstrand = " << ST
              << "\nscore = " << bitScore << "\nnumIdent = " << numIdent
              << "\neVal = " << eValue << "\n1 seq: " << seq1start << " - "
              << seq1end << "\n2 seq: " << seq2start << " - " << seq2end
              << "\nalignLength = " << alignlength << "\n";
  }
};

// Hash-map implementation of adjustment list (vertex id -> edge-related data)
typedef dense_hash_map<long int, EdgeInfo *> AdjMap;
// Hash-map based labels set stored by each node (label id -> coeff)
typedef dense_hash_map<long int, double> LabelsMap;

template <typename T>
inline T getValue(const T &t) {return t;}

template <typename T1, typename T2>
inline T2 getValue(const std::pair<T1, T2> &obj) {return obj.first;}

// Helper function to check that the first container's content is a subset of
// the second container's content
// Note that container type should provide find() method
template <typename T>
bool isSubset(const T &cont1, const T &cont2) {
  typedef typename T::const_iterator CIter;

  bool issubset = true;
  for (CIter I = cont1.begin(), E = cont1.end();
       I != E && issubset; ++I)
    issubset = (cont2.find(getValue(*I)) != cont2.end());

  return issubset;
}

/// Structure to hold results of makeMBlastFast function
struct EdgeInfoWithSetsNums {
  int set2id, seq1id, seq2id;
  EdgeInfo eInfo;

  EdgeInfoWithSetsNums(int set2id_, int seq1id_, int seq2id_,
                       const EdgeInfo &info_):
      set2id(set2id_), seq1id(seq1id_), seq2id(seq2id_), eInfo(info_) {}
};

#endif	//GRAPHHELPERTYPES_H_

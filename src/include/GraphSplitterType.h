#ifndef GRAPHSPLITTERTYPE_H_
#define GRAPHSPLITTERTYPE_H_

#include "GraphCore.h"

namespace weights {
  // Score is the same as MEGABLAST alignment score.
  template <typename T = double>
  class ScoreWeight {
  public:
    typedef T ScoreType;
    ScoreType getLowerBound() const {return SCORE_THRESHOLD;}
    ScoreType getScore(const EdgeInfo *EI) const {return EI->bitScore;}
  };
  // If E value > DBL_MIN score is the same as the MEGABLAST score, 0 otherwise.
  template <typename T = double>
  class ThresholdScoreWeight {
  public:
    typedef T ScoreType;
    ScoreType getLowerBound() const {return SCORE_THRESHOLD;}
    ScoreType getScore(const EdgeInfo *EI) const {
      if (EI->eValue > DBL_MIN)
        return 0.0;
      return EI->bitScore;
    }
  };
  // Score is the alignment length including gaps.
  template <typename T = unsigned int>
  class AlignLengthWeight {
  public:
    typedef T ScoreType;
    ScoreType getLowerBound() const {return 0;}
    ScoreType getScore(const EdgeInfo *EI) const {return EI->alignlength;}
  };
} // namespace weights

// Typedef for external instantiation of graph clustering template classes
typedef weights::ScoreWeight<> EdgeWeightCounter;
typedef LabelRank<EdgeWeightCounter> GraphSplittingInst;
#endif	//GRAPHSPLITTERTYPE_H_

#ifndef CONTIGS_H_
#define CONTIGS_H_

#include <iostream>
#include <string>
#include <cassert>

#include "../src/util/sequtil/sequtil_shared.hpp"
#include "../src/util/sequtil/sequtil_tables.hpp"

using std::cout;
using std::string;

class Contig {
private:
  string name;
  string seq;
  long len;
  int assemblyTag;

public:
  Contig(): name(""), seq(""), len(-1), assemblyTag(-1) {}
  Contig(string name_, string seq_, int assemblyTag_);
  // Merging contigs constructor.
  Contig(const Contig &c1,const Contig &c2, int seq1start, int seq1end,
         int seq2start, int seq2end, int tag);
  ~Contig();
  void extendRight(const Contig &c, int seq1start, int seq1end, int seq2start,
                   int seq2end);
  void extendLeft(const Contig &c, int seq1start, int seq1end, int seq2start,
                   int seq2end);
  void trimFromLeft(int seq1start, int seq1end);
  void trimFromRight(int seq1start, int seq1end);
  bool isValid() const {return len != -1;}
  string getName() const {return name;}
  const string& getRefName() const {return name;}
  void setName(const string &name) { this->name =  name; }
  string getSeq() const {return seq;}
  const string& getRefSeq() const {return seq;}
  // Return non-const sequence. Try to avoid calling this method.
  string& getBareSeq() {return seq;}
  long getLen() const {return len;}
  int getTag() const {return assemblyTag;}
  void setTag(int Tag) {assemblyTag = Tag;}
  // Generate reverse-complement contig.
  Contig getRC() const;
  // Debug function.
  void dump() const;
};

class ContigSortPred {
public:
  struct naivePred {
    bool operator ()(const Contig &c1, const Contig &c2) {
      return (c1.getLen() < c2.getLen());
    }
  };
  struct naiveReversePred {
    bool operator ()(const Contig &c1, const Contig &c2) {
      return (c1.getLen() > c2.getLen());
    }
  };
  struct tagPred {
    bool operator ()(const Contig &c1, const Contig &c2) {
      return (c1.getTag() < c2.getTag());
    }
  };
};

#endif //CONTIGS_H_

#ifndef BLASTRUNNER_H_
#define BLASTRUNNER_H_

#include "GraphHelperTypes.h"
#include <string>
#include <ncbi_pch.hpp>

// Objects includes
#include <objects/seqloc/Seq_loc.hpp>
#include <objects/seqalign/Score.hpp>
#include <objects/seqalign/Score_.hpp>

// Object Manager includes
#include <objtools/simple/simple_om.hpp>

// BLAST includes
#include <algo/blast/api/bl2seq.hpp>
#include <algo/blast/api/sseqloc.hpp>
#include <algo/blast/blastinput/blast_input.hpp>
#include <algo/blast/blastinput/blast_fasta_input.hpp>
#include <algo/blast/api/objmgr_query_data.hpp>

using namespace ncbi;
using namespace blast;

/// Accumulate parsed CSeqAlign objects for pairwise alignment. Stores bases'
/// coverage for query and db sequences and consequence of parsed CSeqAlign
/// objects. Normally, overlapping or covering events results in single
/// CSeqAlign but in some more complex cases (such as indels or nearly perfect
/// covering) need to analyze the whole CSeqAlign objects consequence.
class AlignStatsKeeper {
public:
  // Representation of covered bases in sequence.
  // TODO: Consider vector<bool> or custom variable-size bitset.
  typedef std::vector<unsigned char> SeqCoveredBases;

private:
  // Query and database length.
  unsigned int qlen, dblen;
  // Flag is raised if align type is clear after another one CSeqAlign.
  bool DecisionIsMade;
  // Points to the last valuable EdgeInfo.
  const EdgeInfo *RelevantEI;
  // Points to the object constructed by constructAlmostCoveredEI().
  mutable EdgeInfo *ConstructedEI;
  // Covered bases in query and database sequences.
  SeqCoveredBases query, db;
  // Stores consequence of parsed CSeqAlign (to EdgeInfo) objects in the same
  // order as it is provided by Blast.
  std::vector<EdgeInfo> AlignList;

public:
  AlignStatsKeeper(unsigned int qlen, unsigned int dblen);
  ~AlignStatsKeeper();
  // Decide to track or not to track the alignment. If track, returns true, EI
  // points to ready to save EdgeInfo object.
  bool getFinalDecision(const EdgeInfo *&EI) const;
  // Feed another EdgeInfo.
  void addAnotherEdgeInfo(const EdgeInfo &EI);
  // Generate EdgeInfo object from the set of EdgeInfo objects in case of almost
  // covered event.
  void constructAlmostCoveredEI(EdgeInfo::EdgeType ET) const;
  // Generate EdgeInfo object from the set of EdgeInfo objects in case of
  // partial coverage.
  bool constructPartialCoverageEI() const;
  bool isPossibleToMerge(const EdgeInfo &EI) const;
  bool isPossibleToCover(EdgeInfo &EI) const;
  // Check if AlignList consequence could be interpreted as almost coverage
  // situation.
  std::pair<bool, EdgeInfo::EdgeType> isPossibleToAlmostCover() const;
};

class BlastRunnerBase;

//Abstract class providing BLAST search implementation;
//run BLAST between query file with set1id against database file with set2id
class BlastImplInterface {
public:
  virtual std::vector<EdgeInfoWithSetsNums>*
      makeMBlast(int set1id, int set2id,
                 const vector<float>& avcontiglens,
                 const BlastRunnerBase& BRB) = 0;
};

//File with queries is partitioned into small parts and CLocalBlast object is
//created for every batch so the peak RAM usage is reasonable
class MemoryOptimized : public BlastImplInterface {
public:
  virtual std::vector<EdgeInfoWithSetsNums>*
      makeMBlast(int set1id, int set2id,
                 const vector<float>& avcontiglens,
                 const BlastRunnerBase& BRB);
};

//All sequences in query file are contemporary stored in memory avoiding
//CLocalBlast objects creation overhead
class SpeedOptimized : public BlastImplInterface {
public:
  virtual std::vector<EdgeInfoWithSetsNums>*
      makeMBlast(int set1id, int set2id,
                 const vector<float>& avcontiglens,
                 const BlastRunnerBase& BRB);
};

//Сlass for performing blast search.
class BlastRunnerBase {
  friend class MemoryOptimized;
  friend class SpeedOptimized;
protected:
  unsigned int setsNum;
  SDataLoaderConfig dlconfig;
  CBlastInputSourceConfig *iconfig;
  //query field is used to store sequences from file with queries
  mutable blast::TSeqLocVector query;
  //db field is used by makePairwiseMBlast function (now depricated)
  blast::TSeqLocVector db;
  mutable CRef<CObjectManager> objmgr;
  CRef<CBlastOptionsHandle> opts_handle;
  //stores BLAST databases found in input directory
  std::vector<CSearchDatabase*> dbsVector;
  //stores prelimanary created objects needed for IQueryFactory
  mutable std::vector<CNcbiIfstream*> ncbiIfstreamVector;

  // Stores inclusion threshold for almost-include cases.
  static const float *inclThreshold;
public:
  //specify DataLoader for nucleotide sequences
  BlastRunnerBase(unsigned int setsNum_): setsNum(setsNum_), dlconfig(false) {};
  virtual ~BlastRunnerBase();
  //prepare BLAST-specific objects
  int initScopes(const vector<string>& setsfullnames);
  // Check that one of the sequences is contained in another.
  static bool isCovered(EdgeInfo &, const unsigned int,
                        const unsigned int);
  //check that sequences are overlapping by their ends
  static bool isPossibleToMerge(const EdgeInfo &, const unsigned int,
                                const unsigned int);
  // Obtain BLAST alignments statistics from Score and CSeq_align objects.
  static void getScoreStats(const CRef<CSeq_align>, EdgeInfo *);
  // Inclusion threshold getter. Return 0 if threshold is not allocated.
  static float getInclThreshold();
  //NB! Now depricated due to poor performance;
  //BLAST 2 sequences using MEGABLAST algorithm.
  //CBl2Seq object is locally created for every search
  EdgeInfo makePairwiseMBlast(const string &queryStr, const string &dbStr);
  //run BLAST between query file with set1id against database file with set2id
  virtual std::vector<EdgeInfoWithSetsNums>*
      makeMBlast(int set1id, int set2id, const vector<float>& avcontiglens);
};

//Argument of template class specifies BLAST search implementation
template <class T>
class BlastRunnerImpl : public BlastRunnerBase {
  T blastImpl;
public:
  BlastRunnerImpl(unsigned int setsNum_, float inclThr)
    : BlastRunnerBase(setsNum_) {
    BlastRunnerBase::inclThreshold = new float(inclThr);
  };
  virtual std::vector<EdgeInfoWithSetsNums>*
      makeMBlast(int set1id, int set2id, const vector<float>& avcontiglens) {
    return blastImpl.makeMBlast(set1id, set2id, avcontiglens, *this);
  }
  ~BlastRunnerImpl() {
    delete BlastRunnerBase::inclThreshold;
    BlastRunnerBase::inclThreshold = NULL;
  }
};

#endif //BLAST_RUNNER_H

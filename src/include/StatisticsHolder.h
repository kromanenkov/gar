#ifndef STATISTICSHOLDER_H_
#define	STATISTICSHOLDER_H_

#include <cfloat>
#include <vector>
#include <iostream>
#include <fstream>

#include "Contigs.h"
#include "GraphCore.h"
#include "ContigsBank.h"

using std::vector;
using std::cout;
using std::ofstream;

class StatisticsHolder {
private:
  long int vertexNum, edgeNum, maxDegree, minDegree;
  unsigned long int inclusionsNum, minInclusions, maxInclusions, partialNum;
  double averageDegree, averageInclusions;
  vector<unsigned int> setsSizes;
  vector<float> avInputLens;
  vector<int> maxInputLens;
  vector<int> minInputLens;
  vector<long> summaryInputLens;
  vector<string> setsNames;
  bool needDetails;
public:
  StatisticsHolder(bool needDetails_ = false): needDetails(needDetails_) {}
  void setSetsSizes(vector<unsigned int> sS) {
    setsSizes = sS;
    unsigned size = sS.size();
    avInputLens.resize(size);
    maxInputLens.resize(size);
    minInputLens.resize(size);
    summaryInputLens.resize(size);
  }
  void setSetsNames(vector<string> sN) {setsNames = sN;}
  const vector<unsigned int>& getSetsSizes() {return setsSizes;}
  const vector<float>& getAvInputLens() {return avInputLens;}
  const double getAvCoveredNum() const { return averageInclusions; }
  void printInputStats(const vector <vector<Contig> > &contigsSets);
  void printScoreMatrixStats(
      const vector<vector<BlastScores::SparseMatrix> > &matr);
  void printContigGraphStats(const ContigGraph &contigGraph,
                             const ContigsBank::ContigsSetsTy &contigsSets,
                             const std::string &outDirName);
  void dumpContigGraph(const ContigGraph &contigGraph,
                       const vector< vector<Contig> > &contigsSets,
                       const std::string &outDirName);
  void dumpGraphVertex(const Vertex &V, long int vertexId, ofstream &outFile);
};

#endif	// STATISTICSHOLDER_H

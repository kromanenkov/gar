#ifndef ARGUMENTPARSER_H_
#define ARGUMENTPARSER_H_

#include <string>
#include "optionparser.h"

using std::string;

class ArgumentHolder {
private:
  enum  optionIndex { UNKNOWN, INDIR, OUTDIR, INCL_THR, PART_COV_THR, STATS,
                      DUMP, LOAD, HELP };
  bool showStat, dump, load;
  string inDirName, outDirName;
  string scoresDumpDirName, scoresLoadDirName;
  bool showHelp;
  float inclThr;
  double partCovThr;

  void setInDirName(const char *);
  void setOutDirName(const char *);
  void setScoresDumpDirName(const char *);
  void setScoresLoadDirName(const char *);
public:
  ArgumentHolder(int argc, char **argv);
  const string& getInDirName() const {return inDirName;}
  const string& getOutDirName() const {return outDirName;}
  const string& getScoresDumpDirName() const {return scoresDumpDirName;}
  const string& getScoresLoadDirName() const {return scoresLoadDirName;}
  bool getShowStat() const {return showStat;}
  bool getDump() const {return dump;}
  bool getLoad() const {return load;}
  bool getShowHelp() const {return showHelp;}
  float getInclThr() const { return inclThr; }
  double getPartCovThr() const { return partCovThr; }
};

#endif //ARGUMENTPARSER_H_

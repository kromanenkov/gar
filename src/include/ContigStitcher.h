#ifndef CONTIGSTITCHER_H_
#define	CONTIGSTITCHER_H_

#include "ContigsBank.h"
#include "GraphCore.h"
#include "ArgumentParser.h"
#include "StatisticsHolder.h"
#include "GraphSplitterType.h"
#include "CoverageManager.h"

/// Merge contigs with the same label after clustering step
class ContigStitcher {
  typedef AdjacencyListImpl::ExtensionArray ExtArrayTy;
  typedef set<long int> ClusterTy;
  typedef const map<long int, ClusterTy> ClustersMapTy;
  typedef AdjMap::const_iterator ConstNghbrIter;
  // Container with merged vertex numbers in order of extension.
  typedef vector<long int> VIdOrderedContainer;
  // Quick access container with merged vertex numbers.
  typedef dense_hash_set<long int> VIdFastContainer;
  // Worklist type for storing order of clusters processing.
  typedef std::list<long int> ClusterWLTy;

  /// Keeps direction information for contig extension.
  struct Direction {
    enum Type {
      Inbound = 0,
      Outbound = 1
    } direction;

    Direction(Type direction) : direction(direction) {}
    void flip() {
      direction = static_cast<Type>(!static_cast<bool>(direction));
    }
    bool isInbound() const { return direction == Direction::Inbound; }
    bool isOutbound() const { return direction == Direction::Outbound; }
    bool operator== (const Direction &d) const {
      return direction == d.direction;
    }
  };

  // Reference to input contigs and contig graph object. Note that they are not
  // const as stitching process implies modifying some of them
  ContigsBank &cBank;
  ContigGraph &cGraph;
  // Structure to hold clustering result
  ClustersMapTy &cMap;
  // Found in each cluster root nodes of coverage tree and start stitching
  // process from them.
  CoverageManager CM;
  // Result contigs set
  ContigsBank contigsToWrite;
  // Tag for result contigs set in ContigsToWrite
  mutable int Tag;
  // Vertexes corresponding inbound and outbound sequences in clusters after
  // merging step
  mutable VIdFastContainer inboundEnds, outboundEnds;
  // Stitching contigs in clusters processing sequence.
  mutable ClusterWLTy clustersWorklist;

  // Root nodes of coverage tree. Should be stitched in the first place.
  CoverageManager::RootsSetTy RS;
  // Check if vertex is covered by any unmerged root node of coverage tree.
  bool isCoveredByRootVertex(long int vrtxid) const;

  /// Helper class for clusters comparing based on their total inclusions
  /// number.
  class InclusionComparator {
    const ContigStitcher *CS;
  public:
    InclusionComparator(const ContigStitcher *CS) : CS(CS) {}
    bool operator() (const long int &clustid1, const long int &clustid2) const;
  };

  // Check that this overlappig could extend contig in Vertex v to the left
  bool isLeftExtension(const Vertex &v, ConstNghbrIter CNI) const;
  // Check that this overlappig could extend contig in Vertex v to the right
  bool isRightExtension(const Vertex &v, ConstNghbrIter CNI) const;
  // Choose best extension in container, returns corresponding vertex's iterator
  template <typename T, typename U>
  ConstNghbrIter selectBestExtension(const T &cont, const U &selector,
                                     const Vertex &v) const;
  // Attemp to extend seed contig in specified direction using sequences from
  // the same cluster.
  VIdOrderedContainer extendInDirection(
      const Vertex *seed, VIdFastContainer *mergeset, Contig *C,
      const ClusterTy &cluster, const Direction &InitDir,
      Direction &FinalDir) const;
  // Returns vertex's neighbours from the contiguous clusters for specified
  // direction.
  VIdFastContainer getTrimCandidatesFromDirection(
      const Vertex &v, const ClusterTy &cluster,
      const Direction &Direction) const;
  // Attemp to exclude overlapping between contigs from different clusters to
  // reduce duplication ratio
  void trimContig(Contig *C, const VIdOrderedContainer &InboundExt,
                  const VIdOrderedContainer &OutboundExt,
                  const ClusterTy &cluster, const Direction &InboundDir,
                  const Direction &OutboundDir) const;
  // Returns vertex's neighbours in same cluster for specified direction.
  VIdFastContainer getPotentialDirectedExtension(
      const Vertex &v, const ClusterTy &cluster,
      const Direction &Direction) const;
  // Stitch pair of contigs in one cluster if it is possible (debug purposes)
  // Returned Contig object should be deleted manually. Depricated!
  Contig* stitchContigsPairInCluster(const ClusterTy &cluster) const;
  // Stitch contigs in one cluster while it is possible, starting from seed
  // vertex. Returned Contig object should be deleted manually.
  Contig* stitchContigsInCluster(long int SeedId, const ClusterTy &cluster);
  // Count summary inclusions number from cluster's contigs.
  unsigned long int getInclusionsNumInCluster(const ClusterTy &cluster) const;
  // Specify order of clusters processing for contigs stitching.
  void fillClustersWorklist() const;
  // get contig in cluster with largest overlapping. Depricated!
  ConstNghbrIter getNextOverlappedVertex(const Vertex &V,
                                         const ClusterTy &cluster) const;
  // get const vertex with max len contig
  template <typename T>
  const Vertex* getMaxLenVertex(T StartIter, T EndIter) const;
  // get vertex with max len contig
  template <typename T>
  Vertex* getMaxLenVertex(T StartIter, T EndIter);
public:
  ContigStitcher(ContigsBank &cBank_, ContigGraph &cGraph_,
                 const ClustersMapTy &cMap_, const ArgumentHolder *AH)
      : cBank(cBank_), cGraph(cGraph_), cMap(cMap_),
        CM(cBank_, cGraph_, cMap_, AH->getPartCovThr()),
        contigsToWrite(AH), Tag(101) {
    inboundEnds.set_empty_key(-1);
    outboundEnds.set_empty_key(-1);
  }
  // Stitch contigs in all cluster (excluding vertexes with no cluster label).
  void stitchContigsInAllClusters();
  // Process root vertexes left unmerged after contigs stitching step. Add
  // root vertex to final set if one of its covered vertexes has cluster label
  // or number of covered vertexes is greater than threshold.
  // TODO: Add option to control it, e.g. --all to add all isolated vertexes.
  void addIsolatedRootVertexes(unsigned int threshold);
  // Write stitched contigs to file with specified path prefix.
  void writeResults(const std::string &outFilePred) const;
  // Get contig from ContigGraph Vertex.
  const Contig& vertex2Contig(const Vertex &v) const {
    return cBank.getContigsSet(v.setId)[v.contigId];
  }
  // Get contig from ContigGraph Vertex.
  Contig& vertex2Contig(Vertex &v) const {
    return cBank.getContigsSet(v.setId)[v.contigId];
  }
};

#endif	// CONTIGSTITCHER_H_

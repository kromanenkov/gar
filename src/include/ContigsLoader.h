#ifndef CONTIGSLOADER_H_
#define CONTIGSLOADER_H_

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>
#include <errno.h>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>

#include "ArgumentParser.h"
#include "Contigs.h"

using std::cout;
using std::ifstream;
using std::ofstream;
using std::string;
using std::vector;

class ContigsLoader {
private:
  string dirname;
  string filename;
  ifstream file;
  DIR *dp;
  struct dirent *dirp;
  struct stat st;
  vector<string> fileNames;
  //Save filenames for StatisticsHolder as fileNames vector will be empty
  //by the end of contigs loading
  vector<string> fileNamesCopy;
  string name, seq;
  int assemblyTag;
  const ArgumentHolder *AH;

public:
  ContigsLoader(const ArgumentHolder *AH_): name(""), seq(""), AH(AH_) {}
  ~ContigsLoader();
  //check if directory name is valid and save appropriate filenames
  //(LINUX compatible)
  bool isAbleToLoad();
  //consequently read contigs from files in directory
  Contig* getContig();
  //unnessesary function to avoid storing object of ArgumentHolder by
  //ContigsBank
  const string& getOutDirName() const {return AH->getOutDirName();}
  //unnessesary function to avoid storing object of ArgumentHolder by
  //ContigsBank
  const string& getInDirName() const {return AH->getInDirName();}
  //unnessesary function to pass to StatisticsHolder object the list of
  //filenames
  const vector<string>& getSetsNames() {return fileNamesCopy;}
};

#endif //CONTIGSLOADER_H_

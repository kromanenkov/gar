#ifndef GRAPHCORE_H_
#define	GRAPHCORE_H_

#include <vector>
#include <map>
#include <set>
#include <fstream>
#include <sstream>
#include <sys/time.h>
#include <unistd.h>
#include <cmath>

#include "Contigs.h"
#include "BlastRunner.h"
#include "GraphHelperTypes.h"
#include "ContigsBank.h"

#include <ext/hash_set>

using std::vector;
using std::pair;
using std::map;
using std::set;
using std::ifstream;
using std::ofstream;
// using __gnu_cxx::hash;
// using tr1::hash;

/// Structure for storing all to all pairwise blast search.
/// Holds 3D score matrix (set1 x set2 x pair(id, id))
struct BlastScores {
  struct EqualPairId {
    bool operator() (const pair<int, int>& id1,
                     const pair<int, int>& id2) const {
      return (id1.first == id2.first && id1.second == id2.second);
    }
  };

  template <typename T> struct HashPairId {
    size_t operator() (const pair<T, T>& p) const {
      size_t h1 = std::hash<T>()(p.first);
      size_t h2 = std::hash<T>()(p.second);
      size_t hash = (h1 << 1) + h1 + h2; //3 * h1 + h2
      return hash;
    }
  };

  typedef pair<int, int> PairId;
  typedef sparse_hash_map<PairId, EdgeInfo, HashPairId<int>, EqualPairId>
    SparseMatrix;
  vector<vector<SparseMatrix> > matrix;
};

/// This class acts as AdjMap object, also it operates with ExtensionArray
/// objects to store vertex's inbound and outbound neighbors.
class AdjacencyListImpl {
public:
  // Defines type for container of vertex's right and left neighbors' ids.
  // It doesn't simply store Vertex objects as trimming and extension works in
  // terms of raw ids.
  typedef std::vector<long int> ExtensionArray;

  AdjacencyListImpl() : InboundNum(0), OutboundNum(0) {
    adjMap.set_empty_key(-1);
    adjMap.set_deleted_key(-2);
  }

  // Supporting AdjMap interface.
  EdgeInfo*& operator[] (long int id) { return adjMap[id]; }
  AdjMap::const_iterator begin() const { return adjMap.begin(); }
  AdjMap::const_iterator end() const { return adjMap.end(); }
  AdjMap::iterator begin() { return adjMap.begin(); }
  AdjMap::iterator end() { return adjMap.end(); }
  AdjMap::const_iterator find(long int id) const { return adjMap.find(id); }
  AdjMap::iterator find(long int id) { return adjMap.find(id); }
  long unsigned int size() const { return adjMap.size(); }
  size_t erase(long int id) { return adjMap.erase(id); }
  void rehash(long unsigned int hint) { adjMap.rehash(hint); }
  // Use only for debug purposes.
  operator AdjMap&() { return adjMap; }
  // Use only for debug purposes.
  operator const AdjMap&() const { return adjMap; }

  // Vertexes appropriate for extension vertex's contig in specified direction.
  // Neighbors in adjMap are splitted into these two groups. Vertexes from
  // inbound group could extend contig in sequence-reverse direction. Vertexes
  // from outbound group could extend contig in sequence-along direction.
  ExtensionArray InboundNeighbors, OutboundNeighbors;
  unsigned int InboundNum, OutboundNum;

private:
  // Adjacency list (vertex id -> edge-related data).
  AdjMap adjMap;
};

/// Graph vertex. Stores set and contig ids, map of adjustment vertexes, map of
/// ids of vertexes which contigs are covered by vertex's contig and vector of
/// label coefficients.
struct Vertex {
  int setId, contigId;
  // Required by stitching process. True if vertex's contig get into result.
  mutable bool isMerged;
  // Structure holding label coefficients at previous step.
  LabelsMap prevLabels;
  // Structure holding label coefficients at current step.
  LabelsMap currLabels;
  // adjMap size.
  long int neighborNum;
  // coveredVIdMap size.
  long unsigned int coveredNum;
  // Sum of weights of vertex's edges.
  double totalEdgeWeight;
  // Cluster label. Should be set during the GraphSplitterBase::runClustering().
  long int clusterId;
  // Adjacency list (vertex id -> edge-related data).
  AdjacencyListImpl adjMap;
  // Map of ids of vertexes which contigs are covered by vertex's contig.
  // (vertex id -> edge-related data).
  AdjMap coveredVIdMap;
  // Map of ids of vertexes which contigs are covering the vertex's contig.
  // (vertex id -> edge-related data).
  AdjMap parentVIdMap;
  // Map of ids of vertexes which contigs share contiguous parts with vertex.
  // (vertex id -> edge-related data).
  AdjMap partlyCoveredVIdMap;

  Vertex(): setId(-1), contigId(-1), isMerged(false), neighborNum(0),
            coveredNum(0), totalEdgeWeight(0.0), clusterId(-1) {}
  Vertex(int setId_, int contigId_): setId(setId_), contigId(contigId_),
      isMerged(false), neighborNum(0), coveredNum(0), totalEdgeWeight(0.0),
      clusterId(-1) {
    prevLabels.set_empty_key(-1);
    currLabels.set_empty_key(-1);
    prevLabels.set_deleted_key(-2);
    currLabels.set_deleted_key(-2);
    coveredVIdMap.set_empty_key(-1);
    parentVIdMap.set_empty_key(-1);
    partlyCoveredVIdMap.set_empty_key(-1);
  }
  // Raise isMerged flag and flags of all vertexes from coversMap
  void markAsMerged(const vector<Vertex> &vertexes) const {
    isMerged = true;
    for (AdjMap::const_iterator I = coveredVIdMap.begin(),
                                E = coveredVIdMap.end(); I != E; ++I)
      vertexes[I->first].isMerged = true;
  }
  // Returns true if vertex has neither neighbors nor covered vertexes.
  bool isIsolatedWithoutInclusion() const {
    return neighborNum == 0 && coveredNum == 0;
  }
  // Returns true if vertex has only covered vertexes.
  bool isIsolatedWithInclusions() const {
    return neighborNum == 0 && coveredNum > 0;
  }
  // Returns true if vertex hasn't got any neighbors.
  bool isIsolated() const { return neighborNum == 0; }
  // Returns true if vertex contained in some cluster.
  bool hasClusterLabel() const { return clusterId != -1; }
  // Check if the overlapping could extend vertex in sequence-reverse direction.
  bool hasInboundExtension(
      const Vertex &v, const EdgeInfo *EI,
      const ContigsBank::ContigsSetsTy &contigsSets) const;
  // Check if the overlapping could extend vertex in sequence-along direction.
  bool hasOutboundExtension(
      const Vertex &v, const EdgeInfo *EI,
      const ContigsBank::ContigsSetsTy &contigsSets) const;
  ~Vertex() {}
};

/// Graph implementation. Contigs - vertexes, overlaps between them - edges.
struct ContigGraph {
  long int vertexNum;
  std::vector<Vertex> vertexes;

  typedef dense_hash_map<BlastScores::PairId, long int,
                         BlastScores::HashPairId<int>, BlastScores::EqualPairId>
      Vertex2IdMap;
  // pair (setid; contigid) -> index of vertex in vertexes
   Vertex2IdMap vrtx2Id;

  ContigGraph(): vertexNum(0) {
    vrtx2Id.set_empty_key(make_pair(-1, -1));
  }
  ~ContigGraph() {}
};

/// Functor for saving and restoring sparse_hash_map object to file stream
class SparseMatrixSerializer {
public:
  bool operator() (ofstream*, const pair<BlastScores::PairId, EdgeInfo>&) const;
  bool operator() (ifstream*, pair<const BlastScores::PairId, EdgeInfo>*) const;
};

class GraphBuilder {
private:
  vector<unsigned int> setsSizes;
  const double scoreThreshold;
  unsigned int setsNum;
  BlastRunnerBase *blastRunner;
  BlastScores scoreMatrix;
  ContigGraph cGraph;

  //dealloc sparse matrix data structures
  void deallocScoreMatrix();

public:
  GraphBuilder(vector<unsigned int> &setsSizes_, float inclThr)
    : setsSizes(setsSizes_), scoreThreshold(SCORE_THRESHOLD) {
    setsNum = setsSizes.size();
    blastRunner = new BlastRunnerImpl<MemoryOptimized>(setsNum, inclThr);
    cout << "Initialize BlastRunner object with inclusion threshold="
         << BlastRunnerBase::getInclThreshold() << ";\n";
  }
  ~GraphBuilder();
  ContigGraph& getContigGraph() { return cGraph; }
  const BlastScores& getScoreMatrix() { return scoreMatrix; }
  //prepare containers for storing sparse matrix
  int allocScoreMatrix();
  //fill score matrix with results of contigs
  //pairwaise comparison with MEGABLAST algorithm
  void fillScoreMatrix(const ContigsBank::ContigsSetsTy &contigsSets,
                       const string indirname, const vector<string> &setsnames,
                       const vector<float> &avcontiglens);
  //dump all score matrixes to disk
  int dumpScoreMatrix(string outPred = "./tmp/set");
  //load all score matrixes from disk, no error checking assuming that
  //dimensions of loaded matrixes correspond to input contigs sets
  // FIXME: Would be a good thing to add check for this
  int loadScoreMatrix(string inPred = "./tmp/set");
  //build contig graph
  int buildGraph(const ContigsBank::ContigsSetsTy &contigsSets);
};

/// Interface for contig graph clustering algorithm
/// CRTP is used to avoid runtime virtual call overhead
template <class ClusteringImpl>
class GraphSplitterBase {
protected:
  long int clustersNum;
  // FIXME: do I need something more lightweight?
  // map from label id to set of vertexes ids
  map<long int, set<long int> > clusterLabels;
  ContigGraph &cg;

public:
  typedef AdjMap::iterator NghbrIter;
  typedef AdjMap::const_iterator ConstNghbrIter;
  typedef LabelsMap::iterator VertexLabelIter;
  typedef LabelsMap::const_iterator ConstVertexLabelIter;

  GraphSplitterBase(ContigGraph &cg_) : clustersNum(-1), cg(cg_) {
    assert(cg_.vertexNum == static_cast<long int>(cg_.vertexes.size()));
  }
  virtual ~GraphSplitterBase() {}
  // FIXME: Either base or derived class owns this method, not both!
  // make initial label distribution
  void initializeLabels() {
    ClusteringImpl &obj = static_cast<ClusteringImpl &>(*this);

    obj.initializeLabels();
  }

  int runClustering() {
    ClusteringImpl &obj = static_cast<ClusteringImpl &>(*this);

    return obj.runClustering();
  }

  long int getClustersNum() const {return clustersNum;}
  const map<long int, set <long int> >& getClusterLabels() const {
    return clusterLabels;
  }
};

/// Label-rank based weighted graph clustering algorithm. It relies on four
/// operations: propagation, inflation, cutoff and conditional update.
/// CRTP is used to avoid runtime virtual call overhead
template <class WeightPolicy>
class LabelRank : public GraphSplitterBase< LabelRank<WeightPolicy> > {
  using typename GraphSplitterBase< LabelRank<WeightPolicy> >::ConstNghbrIter;
  using typename GraphSplitterBase< LabelRank<WeightPolicy> >::NghbrIter;
  using typename
      GraphSplitterBase< LabelRank<WeightPolicy> >::ConstVertexLabelIter;
  using typename
      GraphSplitterBase< LabelRank<WeightPolicy> >::VertexLabelIter;

  // specify way of obtaining edge weight
  WeightPolicy weightPolicy;

  // cutoff threshold of cutoff operator
  double threshold;
  // parameter of inflation operator
  double inflationPower;
  // parameter of conditional update operator
  double updateFreq;
  // number of vertexes with updated label distribution on each iteration
  long int changeNum;
  // allowed number of repetions of any changeNum in stopCondition
  int stopFreq;
  // structure used to track stopCondition
  std::map<long int, int> updatedVertexesNum;

  // helper function for conditionalUpdateOperation
  const LabelsMap getMaxLabels(const LabelsMap &lm);
  void normalizeLabels(LabelsMap &lm);
  void propagateOperation();
  void inflationOperation();
  void cutoffOperation();
  void conditionalUpdateOperation();
  bool stopCondition();
  // LabelRank algorithm implies adding selfloop to each vertex. This method
  // restores the initial graph structure.
  void deleteSelfLoops();

public:
  LabelRank(ContigGraph &cg_, double thrshld_, double inflPwr_,
            double updFreq_, int stpFreq_) :
      GraphSplitterBase<LabelRank <WeightPolicy> >(cg_), threshold(thrshld_),
      inflationPower(inflPwr_), updateFreq(updFreq_), changeNum(0),
      stopFreq(stpFreq_) { initializeLabels(); }
  void initializeLabels();
  int runClustering();
};

#endif // GRAPHCORE_H_

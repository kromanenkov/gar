#ifndef COVERAGEMANAGER_H_
#define	COVERAGEMANAGER_H_

#include "GraphCore.h"

/// Build contig coverage tree. Organize contig stitching is such way that
/// root nodes are merged before child nodes.
class CoverageManager {
public:
  // FIXME: Could this be shared with ContigStitcher? But beware of including
  // ContigStitcher.h. Maybe define it in GraphCore.h?
  typedef std::set<long int> ClusterTy;

  /// Stores different coverage-related statistics for cluster.
  class ClusterCoverageInfo {
  public:
    // Container with root nodes in cluster. Sorted by length descending order.
    typedef std::vector<long int> ClusterRootsTy;

    // Underlying data is public because all of it is available from
    // ContigStitcher and CoverageManager is a ContigStitcher's field.
    // Total number of covered contigs by cluster's contigs.
    unsigned long CoverageNum;
    // Total length of covered contigs by cluster's contigs.
    long double CoverageLen;
    // Reference to the actual cluster object.
    const ClusterTy &cluster;

    ClusterCoverageInfo(const ClusterTy &cluster) : CoverageNum(0),
        CoverageLen(0.0), cluster(cluster) {}
    const ClusterRootsTy& getClusterRoots() const { return CR; }
    ClusterRootsTy& getClusterRoots() { return CR; }

  private:
    // Root vertexes in cluster.
    ClusterRootsTy CR;
  };

  // Type of container with root nodes of coverage tree.
  typedef dense_hash_set<long int> RootsSetTy;
  // Map from cluster id to ClusterCoverageInfo.
  typedef std::map<long int, ClusterCoverageInfo> ClusterCoverageMapTy;
  // Type of container with vertexes covered by other vertexes combination.
  typedef std::vector<long int> CoveredByCombinationTy;
  typedef AlignStatsKeeper::SeqCoveredBases SeqCoveredBases;

private:
  // Helper class to keep root vertexes in cluster sorted by length descending
  // order.
  class RootNodesComparator {
    const CoverageManager *CM;
  public:
    RootNodesComparator(const CoverageManager *CM) : CM(CM) {}
    bool operator() (const long int &id1, const long int &id2) const;
  };

  const ContigsBank &CB;
  const ContigGraph &CG;
  // Track coverage of each vertex. 1 - if vertex is covered, 0 - otherwise.
  std::vector<unsigned char> coveredVertexes;
  // Root nodes of coverage tree.
  RootsSetTy roots;
  // Vertexes that are almost fully covered by the combination of other
  // vertexes.
  CoveredByCombinationTy CoveredByCombination;
  // Map from cluster id to its coverage statistics.
  ClusterCoverageMapTy CCM;
  // Threshold for contig's coverage by the combination of other contigs.
  double PartialCoverageThreshold;

  // Fill coveredVertexes array. Uncovered vertexes become root nodes. Track
  // root nodes in each cluster.
  void formRootSet();
  // Fill CoveredByCombination array. Set isMerged flag for such vertexes.
  void formCoveredByCombinationSet();
  // Count different coverage statistics of clusters' contigs.
  void countClustersCoverageStats();
public:
  CoverageManager(const ContigsBank &CB, const ContigGraph &CG,
                  const std::map<long int, ClusterTy > &ClustersMap,
                  double PartCovThr);
  const RootsSetTy& getRoots() const { return roots; }
  RootsSetTy& getRoots() { return roots; }
  const ClusterCoverageMapTy& getClusterCoverageMap() const { return CCM; }
};

#endif	// COVERAGEMANAGER_H_

#ifndef CONTIGSBANK_H_
#define CONTIGSBANK_H_

#include <vector>
#include <algorithm>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>

#include "ContigsLoader.h"
#include "Contigs.h"

// Name of the file with the resulted contigs.
const char* const OutputFilename = "gar.fa";

using std::cout;
using std::vector;
using std::string;
using std::stringstream;

class ContigsBank {
public:
  typedef vector<vector<Contig> > ContigsSetsTy;

private:
  ContigsLoader CLoader;
  ContigsSetsTy contigsSets;
  vector<unsigned int> setsSizes;
  int tagsNum;

public:
  ContigsBank(const ArgumentHolder *AH_): CLoader(AH_), tagsNum(0) {}
  ~ContigsBank();
  // Check if output directory exists. POSIX compatible.
  bool checkOutputDir() const;
  // Write contigs to file(s), numToPrint - maximal limit of written contigs for
  // each set. If MultipleFiles flag is specified, write contigs with different
  // tags to different files.
  void writeContigsSets(int numToPrint = 0, bool MultipleFiles = false) const;
  // Get all contigs
  ContigsSetsTy& getContigsSets() { return contigsSets; }
  // Get all contigs
  const ContigsSetsTy& getContigsSets() const { return contigsSets; }
  // Get contigs from one assembly
  vector<Contig>& getContigsSet(int setNum) { return contigsSets[setNum]; }
  // Get contigs from one assembly
  const vector<Contig>& getContigsSet(int setNum) const {
    return contigsSets[setNum];
  }
  // Get vector with contigs sets sizes
  vector<unsigned int>& getSetsSizes() { return setsSizes; }
  // Save distinct contig
  void addContig(const Contig &C);
  // Read contigs from files in dir
  bool fillContigsSets();
  // Get vector with filenames in input directory
  const vector<string>& getSetsNames() { return CLoader.getSetsNames(); }
  // Get name of input directory
  string getIndirName() { return CLoader.getInDirName(); }
};

#endif //CONTIGSBANK_H_

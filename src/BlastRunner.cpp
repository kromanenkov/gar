#include "include/BlastRunner.h"

// Definition of pointer to static data member.
const float *BlastRunnerBase::inclThreshold;

AlignStatsKeeper::AlignStatsKeeper(unsigned int qlen, unsigned int dblen) :
    qlen(qlen), dblen(dblen), DecisionIsMade(false), RelevantEI(NULL),
    ConstructedEI(NULL) {
  query.resize(qlen, 0);
  db.resize(dblen, 0);

  AlignList.reserve(3);
}

void AlignStatsKeeper::addAnotherEdgeInfo(const EdgeInfo& EI) {
  // Copy parameter as isPossibleToCover() method modifies its argument.
  EdgeInfo EdgeInfoToTest(EI);

  if (isPossibleToCover(EdgeInfoToTest)) {
    // BLAST should provide HSP sorted in descending order of score, so covering
    // event must precide overlap event.
    if (DecisionIsMade) {
      if (EdgeInfoToTest.bitScore > RelevantEI->bitScore) {
        AlignList.push_back(EdgeInfoToTest);
        RelevantEI = &AlignList.back();
        return;
      }
    } else { // isPossibleToCover() == true && !DecisionIsMade
      DecisionIsMade = true;
      AlignList.push_back(EdgeInfoToTest);
      RelevantEI = &AlignList.back();
      return;
    }
  } else if (isPossibleToMerge(EI)) {
    if (DecisionIsMade) {
      if (EI.bitScore > RelevantEI->bitScore) {
        AlignList.push_back(EI);
        RelevantEI = &AlignList.back();
        return;
      }
    } else { // isPossibleToMerge() == true && !DecisionIsMade
      DecisionIsMade = true;
      AlignList.push_back(EI);
      RelevantEI = &AlignList.back();
      return;
    }
  } else { // !isPossibleToCover && !isPossibleToMerge
    if (!DecisionIsMade) {
      AlignList.push_back(EI);
      std::fill(&query[EI.seq1start], &query[EI.seq1end], 1);
      std::fill(&db[EI.seq2start], &db[EI.seq2end], 1);
      return;
    }
  }
}

bool AlignStatsKeeper::isPossibleToCover(EdgeInfo& EI) const {
  bool IsFirstCovered = (EI.seq1end - EI.seq1start) == qlen - 1,
       IsSecondCovered = (EI.seq2end - EI.seq2start) == dblen - 1;

  if (IsFirstCovered && IsSecondCovered) {
    EI.ET = EdgeInfo::Identical;
    return true;
  }

  if (IsFirstCovered) {
    EI.ET = EdgeInfo::SecondCovers;
    return true;
  }
  if (IsSecondCovered) {
    EI.ET = EdgeInfo::FirstCovers;
    return true;
  }

  return false;
}

bool AlignStatsKeeper::isPossibleToMerge(const EdgeInfo& EI) const {
  // TODO: Check if we need to count overlappings with a small shift.
  const unsigned int shift = 0 /*einfo.numIdent >> 2*/;

  bool QueryEnd = EI.seq1end >= (qlen - 1 - shift),
       DbEnd = EI.seq2end >= (dblen - 1- shift),
       QueryStart = EI.seq1start <= shift,
       DbStart = EI.seq2start <= shift;

  switch (EI.ST) {
    case EdgeInfo::PlusPlus: {
      return (QueryEnd && DbStart) || (DbEnd && QueryStart);
    }
    case EdgeInfo::PlusMinus: {
      return (QueryEnd && DbEnd) || (QueryStart && DbStart);
    }
    case EdgeInfo::MinusPlus: {
      return (QueryStart && DbStart) || (QueryEnd && DbEnd);
    }
    case EdgeInfo::MinusMinus: {
      return (QueryStart && DbEnd) || (QueryEnd && DbStart);
    }
  }
  return false;
}

std::pair<bool, EdgeInfo::EdgeType>
    AlignStatsKeeper::isPossibleToAlmostCover() const {
  unsigned int CoveredQueryBases = 0, CoveredDbBases = 0;
  // Blank return value.
  std::pair<bool, EdgeInfo::EdgeType> Result =
      std::make_pair(false, EdgeInfo::Normal);

  // Count covered bases in query sequence.
  for (unsigned int i = 0, e = query.size(); i < e; ++i)
    if (query[i])
      ++CoveredQueryBases;
  // Count covered bases in database sequence.
  for (unsigned int i = 0, e = db.size(); i < e; ++i)
    if (db[i])
      ++CoveredDbBases;

  bool IsFirstAlmostCovered =
           CoveredQueryBases >= qlen * BlastRunnerBase::getInclThreshold(),
       IsSecondAlmostCovered =
           CoveredDbBases >= dblen * BlastRunnerBase::getInclThreshold(),
       IsFirstCovered = CoveredQueryBases == qlen,
       IsSecondCovered = CoveredDbBases == dblen;

  if (!IsFirstAlmostCovered && !IsSecondAlmostCovered)
    return Result;

  // One of the sequences is almost covered at least.
  Result.first = true;
  if (IsFirstCovered && IsSecondCovered) {
    Result.second = EdgeInfo::Identical;
    return Result;
  }

  if (IsFirstCovered) {
    Result.second = EdgeInfo::SecondCovers;
    return Result;
  }
  if (IsSecondCovered) {
    Result.second = EdgeInfo::FirstCovers;
    return Result;
  }

  if (IsFirstAlmostCovered && IsSecondAlmostCovered) {
    if (qlen > dblen)
      Result.second = EdgeInfo::FirstCovers;
    else
      Result.second = EdgeInfo::SecondCovers;
  } else { // IsFirstAlmostCovered || IsSecondAlmostCovered
    if (IsFirstAlmostCovered) {
      Result.second = EdgeInfo::SecondCovers;
    } else
      Result.second = EdgeInfo::FirstCovers;
  }

  return Result;
}

void AlignStatsKeeper::constructAlmostCoveredEI(
    EdgeInfo::EdgeType ET) const {
  ConstructedEI = new EdgeInfo();

  ConstructedEI->ET = ET;
  // In case of changing default constructor behaviour.
  ConstructedEI->eValue = 0;
  double score = 0.0;
  unsigned int alignlength = 0, numident = 0;
  unsigned int MinLen = std::min(qlen, dblen);
  for (unsigned int i = 0, e = AlignList.size(); i < e; ++i) {
    score += AlignList[i].bitScore;
    alignlength += AlignList[i].alignlength;
    numident += AlignList[i].numIdent;
  }

  // Prevent align length from exceeding MinLen.
  unsigned int MinAlignLength = std::min(MinLen, alignlength);
  ConstructedEI->alignlength = MinAlignLength;
  ConstructedEI->bitScore = score;
  ConstructedEI->numIdent = std::min(numident, MinLen);
  ConstructedEI->seq1start = ConstructedEI->seq2start = 0;
  ConstructedEI->seq1end = ConstructedEI->seq2end = MinAlignLength;
}

bool AlignStatsKeeper::constructPartialCoverageEI() const {
  if ((!AlignList.size()) || (AlignList.size() == 1 && RelevantEI) ||
      RelevantEI == &AlignList.front())
    return false;

  // First CSeqAlign acts as seed overlap interval.
  const EdgeInfo &SeedEI = AlignList.front();
  long QRangeStart = SeedEI.seq1start, QRangeEnd = SeedEI.seq1end,
               DBRangeStart = SeedEI.seq2start, DBRangeEnd = SeedEI.seq2end;

  // Contiguously extend seed overlap interval.
  while (QRangeStart - 1 >= 0 && query[QRangeStart - 1])
    --QRangeStart;
  while (QRangeEnd + 1 < qlen && query[QRangeEnd + 1])
    ++QRangeEnd;
  while (DBRangeStart - 1 >= 0 && db[DBRangeStart - 1])
    --DBRangeStart;
  while (DBRangeEnd + 1 < dblen && db[DBRangeEnd + 1])
    ++DBRangeEnd;

  ConstructedEI = new EdgeInfo();
  ConstructedEI->ET = EdgeInfo::PartialCoverage;
  ConstructedEI->seq1start = QRangeStart;
  ConstructedEI->seq1end = QRangeEnd;
  ConstructedEI->seq2start = DBRangeStart;
  ConstructedEI->seq2end = DBRangeEnd;
  ConstructedEI->bitScore = QRangeEnd - QRangeStart + 1;

  return true;
}

bool AlignStatsKeeper::getFinalDecision(const EdgeInfo *&EI) const {
  // The whole EdgeInfo consequence could produce an almost covering situation
  // even if the first CSeqAlign object is parsed as a normal overlapping. So we
  // need to iterate through the whole consequence first.
  std::pair<bool, EdgeInfo::EdgeType> Res = isPossibleToAlmostCover();
  if (Res.first) {
    constructAlmostCoveredEI(Res.second);
    if (DecisionIsMade && RelevantEI->bitScore > ConstructedEI->bitScore)
      EI = RelevantEI;
    else
      EI = ConstructedEI;
    return true;
  } else if (constructPartialCoverageEI()) {
    if (DecisionIsMade && RelevantEI->bitScore > ConstructedEI->bitScore)
      EI = RelevantEI;
    else
      EI = ConstructedEI;
    return true;
  } else if (DecisionIsMade)  {
    EI = RelevantEI;
    return true;
  }

  EI = NULL;
  return false;
}

AlignStatsKeeper::~AlignStatsKeeper() {
  if (ConstructedEI)
    delete ConstructedEI;
}

BlastRunnerBase::~BlastRunnerBase() {
  delete iconfig;
  opts_handle.Reset();

  assert(ncbiIfstreamVector.size() == dbsVector.size() && "Size mismatch");
  for (unsigned i = 0; i < dbsVector.size(); ++i) {
    delete dbsVector[i];
    delete ncbiIfstreamVector[i];
  }
  objmgr.Reset();

  cout << "Free BLAST-related data\n";
}

//prepare BLAST-specific objects
int BlastRunnerBase::initScopes(const vector<string>& setsfullnames) {
  objmgr = CObjectManager::GetInstance();
  if (!objmgr) {
    cout << "Could not initialize object manager";
    return -1;
  }

  if (opts_handle.IsNull())
    opts_handle = CBlastOptionsFactory::Create(eMegablast);
  dlconfig.OptimizeForWholeLargeSequenceRetrieval(true);
  iconfig = new CBlastInputSourceConfig(dlconfig);

  assert(setsNum == setsfullnames.size());

  for (unsigned i = 0; i < setsNum ; ++i) {
    dbsVector.push_back(new CSearchDatabase(setsfullnames[i],
                                            CSearchDatabase::
                                            eBlastDbIsNucleotide));
    ncbiIfstreamVector.push_back(new CNcbiIfstream(setsfullnames[i].data()));
  }

  return 0;
}

// Check that one of the sequences is contained in another.
bool BlastRunnerBase::isCovered(EdgeInfo &einfo, const unsigned int len1,
                                const unsigned int len2) {
  // Early return in case of overlapping.
  if (isPossibleToMerge(einfo, len1, len2))
    return false;

  bool IsFirstAlmostCovered = (einfo.seq1end - einfo.seq1start) >=
                              (len1 - 1) * (*(BlastRunnerBase::inclThreshold)),
       IsSecondAlmostCovered = (einfo.seq2end - einfo.seq2start) >=
                               (len2 - 1) * (*(BlastRunnerBase::inclThreshold)),
       IsFirstCovered = (einfo.seq1end - einfo.seq1start) == len1 - 1,
       IsSecondCovered = (einfo.seq2end - einfo.seq2start) == len2 - 1;

  // Filter out non-inclusion cases.
  if (!IsFirstAlmostCovered && !IsSecondAlmostCovered)
    return false;

  if (IsFirstCovered && IsSecondCovered) {
    einfo.ET = EdgeInfo::Identical;
    return true;
  }

  if (IsFirstCovered) {
    einfo.ET = EdgeInfo::SecondCovers;
    return true;
  }
  if (IsSecondCovered) {
    einfo.ET = EdgeInfo::FirstCovers;
    return true;
  }

  // Ignore almost includes with not zero evalue.
  if (einfo.eValue > CLUSTERING_EPS)
    return false;
  if (IsFirstAlmostCovered && IsSecondAlmostCovered) {
    if (len1 > len2)
      einfo.ET = EdgeInfo::FirstCovers;
    else
      einfo.ET = EdgeInfo::SecondCovers;
  } else { // IsFirstAlmostCovered || IsSecondAlmostCovered
    if (IsFirstAlmostCovered) {
      einfo.ET = EdgeInfo::SecondCovers;
    } else
      einfo.ET = EdgeInfo::FirstCovers;
  }

  return true;
}

// Check that sequences are overlapping by their ends.
// This version refers to GraphBuilder::is[Left|Right]Extension.
bool BlastRunnerBase::isPossibleToMerge(const EdgeInfo& einfo,
                                        const unsigned int len1,
                                        const unsigned int len2) {
  // TODO: Check if we need to count overlappings with a small shift.
  const unsigned int shift = 0/*einfo.numIdent >> 2*/;

  bool queryEnd = einfo.seq1end >= (len1 - 1 - shift),
       dbEnd = einfo.seq2end >= (len2 - 1- shift),
       queryStart = einfo.seq1start <= shift,
       dbStart = einfo.seq2start <= shift;

  // This conditions are duplicated from isCovered() to ban merging of
  // overlapping contigs.
  bool IsFirstCovered =
      (einfo.seq1end - einfo.seq1start) == len1 - 1,
       IsSecondCovered =
      (einfo.seq2end - einfo.seq2start) == len2 - 1;
  // Early return if this SeqAlign will be parsed by isCovered() method.
  if (IsFirstCovered || IsSecondCovered)
    return false;

  switch (einfo.ST) {
    case EdgeInfo::PlusPlus: {
      return (queryEnd && dbStart) || (dbEnd && queryStart);
    }
    case EdgeInfo::PlusMinus: {
      return (queryEnd && dbEnd) || (queryStart && dbStart);
    }
    case EdgeInfo::MinusPlus: {
      return (queryStart && dbStart) || (queryEnd && dbEnd);
    }
    case EdgeInfo::MinusMinus: {
      return (queryStart && dbEnd) || (queryEnd && dbStart);
    }
  }
  return false;
}

// Obtain BLAST alignments statistics from Score and CSeq_align objects.
void BlastRunnerBase::getScoreStats(const CRef<CSeq_align> CSA,
                                    EdgeInfo* eInfo) {
  const CSeq_align_Base::TScore &scoreObj = CSA->GetScore();
  vector< CRef< CScore > > scoreVector(scoreObj);
  //obtain BLAST statistics from alignment
  for (unsigned int i = 0, e = scoreVector.size(); i < e; ++i) {
    const CObject_id& id = scoreVector[i]->GetId();
    if (id.IsStr()) {
      if (id.GetStr() == "bit_score") {
        eInfo->bitScore = scoreVector[i]->GetValue().GetReal();
        if (eInfo->bitScore < SCORE_THRESHOLD) break;
      } else if (id.GetStr() == "e_value" || id.GetStr() == "sum_e") {
          eInfo->eValue = (scoreVector[i])->GetValue().GetReal();
      } else if (id.GetStr() == "num_ident") {
          eInfo->numIdent = (scoreVector[i])->GetValue().GetInt();
      }
    }
  }

  ENa_strand queryStrand = CSA->GetSeqStrand(0),
             dbStrand = CSA->GetSeqStrand(1);

  assert((queryStrand == eNa_strand_plus ||
          queryStrand == eNa_strand_minus) &&
         "Unknown query strand type!");
  assert((dbStrand == eNa_strand_plus || dbStrand == eNa_strand_minus) &&
         "Unknown database strand type!");

  if (queryStrand == eNa_strand_plus) {
    if (dbStrand == eNa_strand_plus)
      eInfo->ST = EdgeInfo::PlusPlus;
    else // dbStrand == eNa_strand_minus
      eInfo->ST = EdgeInfo::PlusMinus;
  } else { // queryStrand == eNa_strand_minus
    if (dbStrand == eNa_strand_plus)
      eInfo->ST = EdgeInfo::MinusPlus;
    else // dbStrand == eNa_strand_minus
      eInfo->ST = EdgeInfo::MinusMinus;
  }

  eInfo->seq1start = CSA->GetSeqStart(0);
  eInfo->seq1end = CSA->GetSeqStop(0);
  eInfo->seq2start = CSA->GetSeqStart(1);
  eInfo->seq2end = CSA->GetSeqStop(1);
  eInfo->alignlength = CSA->GetAlignLength(true);
}

float BlastRunnerBase::getInclThreshold() {
  if (!BlastRunnerBase::inclThreshold) {
    assert(0 && "Memory for inclusion threshold isn't allocated.");
    return 0.0;
  }

  return *(BlastRunnerBase::inclThreshold);
}

//NB! Now depricated due to poor performance
//BLAST 2 sequences using MEGABLAST algorithm. Returns alignment's score
//CBl2Seq object is locally created for every search
//TODO: can we just prepare TSeqLocVector of all sequences before calling blast?
EdgeInfo BlastRunnerBase::makePairwiseMBlast(const string &queryStr, const string &dbStr) {
  EdgeInfo blankInfo;

  CBlastFastaInputSource queryInput(queryStr, *iconfig);
  CBlastFastaInputSource dbInput(dbStr, *iconfig);
  CScope scope1(*objmgr);
  CScope scope2(*objmgr);

  CBlastInput qIn(&queryInput);
  CBlastInput dbIn(&dbInput);

  query = qIn.GetAllSeqLocs(scope1);
  db = dbIn.GetAllSeqLocs(scope2);

  blast::CBl2Seq blaster(query, db, eMegablast);
  blast::TSeqAlignVector alignments = blaster.Run();

  query.clear();
  db.clear();

//  Display the alignments in text ASN.1
//  for (int i = 0; i < (int) alignments.size(); ++i)
//    cout << MSerial_AsnText << *alignments[i];

  //result vector should contain only one Seq-align-set as we perform
  //1 to 1 search
  assert(alignments.size() == 1);
  CRef<CSeq_align_set> CSAS = alignments[0];
  std::list<CRef<CSeq_align> > CSA = CSAS->Get();

  if (CSA.size() == 0) return blankInfo;

  for (std::list<CRef<CSeq_align> >::const_iterator alignI = CSA.begin(),
       alignE = CSA.end(); alignI != alignE; ++alignI) {
    //store current alignment statistics
    EdgeInfo tmpBlankInfo;

    //skip complementary alignments
    //if ((*alignI)->GetSeqStrand(0) != (*alignI)->GetSeqStrand(1)) continue;

    //obtain BLAST statistics from alignment
    getScoreStats(*alignI, &tmpBlankInfo);
    if (tmpBlankInfo.bitScore < SCORE_THRESHOLD) continue;
    tmpBlankInfo.seq1start = (*alignI)->GetSeqStart(0);
    tmpBlankInfo.seq1end = (*alignI)->GetSeqStop(0);
    tmpBlankInfo.seq2start = (*alignI)->GetSeqStart(1);
    tmpBlankInfo.seq2end = (*alignI)->GetSeqStop(1);
    tmpBlankInfo.alignlength = (*alignI)->GetAlignLength(true);
//    if (isPossibleToMerge(tmpBlankInfo, queryStr.length(), dbStr.length()) &&
//        tmpBlankInfo.bitScore > blankInfo.bitScore)
      blankInfo = tmpBlankInfo;
  }

  return blankInfo;
}

std::vector<EdgeInfoWithSetsNums>*
    BlastRunnerBase::makeMBlast(int set1id, int set2id,
                                const vector<float>& avcontiglens) {
  cout << "Warning: using blank makeMBlast function\n";
  return new vector<EdgeInfoWithSetsNums>();
}

std::vector<EdgeInfoWithSetsNums>*
    SpeedOptimized::makeMBlast(int set1id, int set2id,
                               const vector<float>& avcontiglens,
                               const BlastRunnerBase& BRB) {
  if (set1id != set2id + 1)
    return new vector<EdgeInfoWithSetsNums>();

  //Create BLAST-specific objects. Couldn't be moved to initScopes due to
  //unknown reasons
  long seq1id, seq2id;
  CScope scope(*(BRB.objmgr));
  CBlastFastaInputSource queryInput(*(BRB.ncbiIfstreamVector[set1id]),
                                    *(BRB.iconfig));
  CBlastInput qIn(&queryInput);
  BRB.query = qIn.GetAllSeqLocs(scope);
  CRef<IQueryFactory> query_factory(new CObjMgr_QueryFactory(BRB.query));

  vector<EdgeInfoWithSetsNums> *resultVector =
    new vector<EdgeInfoWithSetsNums>();

  for (int set2id = 0 ; set2id < set1id; ++set2id) {
    CLocalBlast blaster(query_factory, BRB.opts_handle,
                        *(BRB.dbsVector[set2id]));
    blaster.SetNumberOfThreads(omp_get_num_procs());
    CRef<CSearchResultSet> results = blaster.Run();
    for (unsigned int i = 0; i < results->GetNumResults(); i++) {
      std::list<CRef<CSeq_align> > CSA = (*results)[i].GetSeqAlign()->Get();

      long prevseq2id = 0;
      //store temporary and final alignment statistics
      EdgeInfo tmpBlankInfo = EdgeInfo(), blankInfo = EdgeInfo();
      for (std::list<CRef<CSeq_align> >::const_iterator alignI = CSA.begin(),
           alignE = CSA.end(); alignI != alignE; ++alignI) {
        //skip complementary alignments
        if ((*alignI)->GetSeqStrand(0) != (*alignI)->GetSeqStrand(1)) continue;
        //get seq ids
        (*alignI)->GetSeq_id(0).GetLocal().GetIdType(seq1id);
        (*alignI)->GetSeq_id(1).GetLocal().GetIdType(seq2id);
        assert(seq1id > 0 && seq2id > 0);

        //check if we have alignment of new pair of sequences
        if (prevseq2id != seq2id) {
          if (blankInfo.bitScore > SCORE_THRESHOLD) {
            resultVector->push_back(EdgeInfoWithSetsNums(set2id, seq1id - 1,
                                                         prevseq2id - 1,
                                                         blankInfo));
            blankInfo = EdgeInfo();
          }
          prevseq2id = seq2id;
        }

        //obtain BLAST statistics from alignment
        BlastRunnerBase::getScoreStats(*alignI, &tmpBlankInfo);
        tmpBlankInfo.seq1start = (*alignI)->GetSeqStart(0);
        tmpBlankInfo.seq1end = (*alignI)->GetSeqStop(0);
        tmpBlankInfo.seq2start = (*alignI)->GetSeqStart(1);
        tmpBlankInfo.seq2end = (*alignI)->GetSeqStop(1);
        tmpBlankInfo.alignlength = (*alignI)->GetAlignLength(true);

        unsigned int qlen = scope.GetSequenceLength((*alignI)->GetSeq_id(0)),
                     dblen = static_cast<unsigned int>(BRB.dbsVector[set2id]->
                       GetSeqDb()->GetSeqLength(seq2id - 1));

//        if (set1id == 6 && set2id == 2) {
//          bool merge = isPossibleToMerge(tmpBlankInfo, qlen, dblen);
//          cout << tmpBlankInfo.seq1start << "-" << tmpBlankInfo.seq1end
//               <<" : " << qlen << "; " << tmpBlankInfo.seq2start << "-" << tmpBlankInfo.seq2end <<" : " << dblen << "; "
//               << " Len = " << tmpBlankInfo.numIdent << "; " << "merge = " << merge << "\n";
//        }

        if (BlastRunnerBase::isPossibleToMerge(tmpBlankInfo, qlen, dblen) &&
            tmpBlankInfo.bitScore > blankInfo.bitScore)
          blankInfo = tmpBlankInfo;

      }
      //push statistics from the last search pair
      if (blankInfo.bitScore > SCORE_THRESHOLD)
        resultVector->push_back(EdgeInfoWithSetsNums(set2id, seq1id - 1,
                                                     seq2id - 1,
                                                     blankInfo));
    }
  }

  BRB.query.clear();
  return resultVector;
}

std::vector<EdgeInfoWithSetsNums>*
    MemoryOptimized::makeMBlast(int set1id, int set2id,
                                const vector<float>& avcontiglens,
                                const BlastRunnerBase& BRB) {
  //Create BLAST-specific objects. Couldn't be moved to initScopes due to
  //unknown reasons
  long seq1id, seq2id;
  CScope scope(*(BRB.objmgr));
  CNcbiIfstream NIFS(BRB.dbsVector[set1id]->GetDatabaseName().data());
  CBlastFastaInputSource queryInput(NIFS, *(BRB.iconfig));
  CBlastInput qIn(&queryInput);

  vector<EdgeInfoWithSetsNums> *resultVector =
    new vector<EdgeInfoWithSetsNums>();

  assert(avcontiglens.size() == BRB.dbsVector.size());
  qIn.SetBatchSize(static_cast<int>(avcontiglens[set1id]) << 5);
  while (!qIn.End()) {
    CRef<CBlastQueryVector> query_batch(qIn.GetNextSeqBatch(scope));
    CRef<IQueryFactory> queries(new CObjMgr_QueryFactory(*query_batch));
    CRef<CSearchResultSet> results;
    CLocalBlast blaster(queries, BRB.opts_handle, *(BRB.dbsVector[set2id]));
//    blaster.SetNumberOfThreads(omp_get_num_procs());
    blaster.SetNumberOfThreads(1);

    results = blaster.Run();

    for (unsigned int i = 0; i < results->GetNumResults(); i++) {
      std::list<CRef<CSeq_align> > CSA = (*results)[i].GetSeqAlign()->Get();
      // Underlying cycle dominators.
      long prevseq2id = 0;
      AlignStatsKeeper *ASK = NULL;

      for (std::list<CRef<CSeq_align> >::const_iterator alignI = CSA.begin(),
           alignE = CSA.end(); alignI != alignE; ++alignI) {
        // Prepare blank object to store CSeq_Align statistics.
        EdgeInfo BlankInfo = EdgeInfo();

        // Get seq ids.
        const CSeq_id_Base::TLocal &QueryIdLocal =
            (*alignI)->GetSeq_id(0).GetLocal();
        if (QueryIdLocal.GetIdType(seq1id) != CObject_id::E_Choice::e_Id) {
          // Local id as string. Introduced in latest BLAST+ releases.
          std::string Label = QueryIdLocal.GetStr();
          static const char Prefix[] = "Query_";
          if (!NStr::StartsWith(Label, Prefix))
            assert(0 && "Wrong sequence local id format");

          seq1id = std::stol(Label.substr(
              sizeof(Prefix) / sizeof(Prefix[0]) - 1, string::npos));
        }
        (*alignI)->GetSeq_id(1).GetLocal().GetIdType(seq2id);
        assert(seq1id > 0 && seq2id > 0);

        // Obtain query and database lengths.
        unsigned int qlen = scope.GetSequenceLength((*alignI)->GetSeq_id(0)),
                     dblen = static_cast<unsigned int>(BRB.dbsVector[set2id]->
                         GetSeqDb()->GetSeqLength(seq2id - 1));

        // Obtain BLAST statistics from alignment.
        BlastRunnerBase::getScoreStats(*alignI, &BlankInfo);

        // Check if we have alignment of a new pair of sequences.
        if (prevseq2id != seq2id) {
          // New alignment pair, expect previous result.
          if (ASK) {
            const EdgeInfo *ResultEI = NULL;
            if (ASK->getFinalDecision(ResultEI)) { // Save resulting EdgeInfo.
              assert(ResultEI && ResultEI->bitScore > SCORE_THRESHOLD);
              resultVector->push_back(EdgeInfoWithSetsNums(set2id, seq1id - 1,
                                                           prevseq2id - 1,
                                                           *ResultEI));
            }
            delete ASK;
          }
          ASK = new AlignStatsKeeper(qlen, dblen);
          if (BlankInfo.bitScore > SCORE_THRESHOLD)
            ASK->addAnotherEdgeInfo(BlankInfo);
          // Update previous seq2idnumber.
          prevseq2id = seq2id;
        } else { // Same alignment pair.
          if (BlankInfo.bitScore > SCORE_THRESHOLD)
            ASK->addAnotherEdgeInfo(BlankInfo);
        }
      }
      //push statistics from the last search pair
      if (ASK) {
        const EdgeInfo *ResultEI = NULL;
        if (ASK->getFinalDecision(ResultEI)) { // Save resulting EdgeInfo.
          assert(ResultEI && ResultEI->bitScore > SCORE_THRESHOLD);
          resultVector->push_back(EdgeInfoWithSetsNums(set2id, seq1id - 1,
                                                       seq2id - 1,
                                                       *ResultEI));
        }
        delete ASK;
      }
    }
  }
  return resultVector;
}

#!/bin/sh

java -jar trimmomatic-0.32.jar PE -phred33 ~/Загрузки/SRR122309/SRR122309_1.fastq.gz ~/Загрузки/SRR122309/SRR122309_2.fastq.gz ~/disser/SRR122309_1_paired.fastq.gz ~/disser/SRR122309_1_unpaired.fastq.gz ~/disser/SRR122309_2_paired.fastq.gz ~/disser/SRR122309_2_unpaired.fastq.gz ILLUMINACLIP:./adapters/TruSeq3-PE-2.fa:2:30:10 TRAILING:20 MINLEN:36 SLIDINGWINDOW:5:25

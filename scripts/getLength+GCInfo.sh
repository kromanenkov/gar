#!/bin/sh

contig_file=$1
contig_num=$2

infoseq $contig_file | sort -nk 6 -r | head -n $contig_num | awk '{ print $5,$6 }'

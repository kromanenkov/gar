#!/usr/bin/env python2

import sys
import fileinput

if len(sys.argv) <= 1:
    print "Provide FASTA file as argument"
    exit(1)

NUM = 1
for line in fileinput.input(sys.argv[1], inplace=1, backup='.bak'):
    if line[0] == '>':
        line = line.replace(">", ">lcl|" + str(NUM) + " ")
        NUM += 1
    sys.stdout.write(line)

#!/bin/sh

scripts_path=~/Disser/gar/scripts
mkdb_path=~/Disser/ncbi-blast-2.6.0+-src/c++/ReleaseMT/bin/

for fname in *.fa *.fasta; do
    [ -f "$fname" ] || continue;
    if [ $# -eq 1 ] && [ "$1" = "-parse_seqid" ]; then
        $scripts_path/formatSeqid.py $fname
    fi
    $mkdb_path/makeblastdb -in $fname -parse_seqids -dbtype nucl;
done

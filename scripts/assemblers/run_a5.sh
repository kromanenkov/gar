#!/bin/sh

queue_name=$1;

#sbatch -N 1 -p $queue_name run ~/_scratch/assembly/soft/a5/bin/a5_pipeline.pl  ../corrected_1_paired.fastq.gz ../corrected_2_paired.fastq.gz srr-a5
sbatch -N 1 -p $queue_name run ~/_scratch/assembly/soft/a5/bin/a5_pipeline.pl my-lib srr-a5
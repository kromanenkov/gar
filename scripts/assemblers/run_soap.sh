#!/bin/sh

queue_num=$1;

sbatch -N 1 -p $queue_num run ~/_scratch/assembly/soft/SOAPdenovo2-src-r240/SOAPdenovo-127mer all -s my_config -K 67 -R -o graph_prefix 1>ass.log 2>ass.err

#!/bin/sh

queue_name=$1;

sbatch -n 1 -p $queue_name run ~/_scratch/assembly/soft/idba-1.1.1/bin/idba_ud --mink 60 --maxk 100 --step 5  -r ./corrected.fasta -o srr-idba3

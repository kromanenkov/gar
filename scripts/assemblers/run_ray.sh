#!/bin/sh

queue_name=$1;

sbatch -n 16 -t 240 -p $queue_name ompi ~/_scratch/assembly/soft/ray-build/Ray -k 49 -p ./corrected_1_paired_.fastq ./corrected_2_paired_.fastq -s ./corrected_unpaired_.fastq  -o srr-ray4
#!/bin/sh

queue_name=$1;

sbatch -N 1 -p $queue_name ompi ~/_scratch/asssembly/soft/abyss-bin/bin/abyss-pe k=63 name=srr in='../corrected_1_paired_.fastq ../corrected_2_paired__.fastq' \
se='../corrected_unpaired_.fastq'

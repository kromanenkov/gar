#!/bin/sh

queue_name=$1;

sbatch -n 1 -p $queue_name -t 360 --ntasks-per-node=1  --export=OMP_NUM_THREADS=8 --export=OMP_THREAD_LIMIT=8 run ~/_scratch/asssembly/soft/velvet_1.2.10/velveth ./srr-velvet/ 59 -shortPaired -separate -fastq ./corrected_1_paired_.fastq ./corrected_2_paired_.fastq -short ./corrected_unpaired_.fastq

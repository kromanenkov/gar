#!/bin/sh

queue_name=$1;

sbatch -N 1 -p $queue_name -t 300 --ntasks-per-node=1  --export=OMP_NUM_THREADS=8 --export=OMP_THREAD_LIMIT=8  run ~/_scratch/asssembly/soft/velvet_1.2.10/velvetg ./srr-velvet/  -ins_length 200 -cov_cutoff auto -exp_cov auto

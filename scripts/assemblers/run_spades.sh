#!/bin/sh

queue_name=$1;

sbatch -N 1 -t 4310 -p $queue_name run ~/_scratch/assembly/soft/SPAdes-3.1.0-Linux/bin/spades.py -k 55,67,77  --careful --pe1-1 ./corrected_1_paired.fastq.gz --pe1-2 ./corrected_2_paired.fastq.gz --pe1-s ./corrected_unpaired.fastq.gz  -o srr-spades3

#!/usr/bin/env python2

# pylint: disable=missing-docstring,too-many-arguments

import argparse
import os
import sys
import logging
import random
import bisect
from itertools import groupby


def parse_args():
    parser =\
        argparse.ArgumentParser(description='Generate de novo assemblies '
                                            'from the reference sequences. '
                                            'Generated files will be named '
                                            'assembly_{%id}.fa')
    parser.add_argument('-r', '--reference', required=True,
                        help='File with reference sequence')
    parser.add_argument('-n', type=int, default=1,
                        help='Number of generated assemblies. Default is 1.')
    parser.add_argument('-o', '--output-dir', default=os.getcwd(),
                        dest='output', help='Directory where generated '
                        'assemblies would be placed. Default is current '
                        'working directory.')
    parser.add_argument('-t', '--trim-freq', dest='trim', default=0.15,
                        help='Trimming frequency for the simulated contig.')
    parser.add_argument('-s', '--sizes-list', dest='sizes', required=True,
                        nargs='+', type=float, help="A set of simulated "
                        "contigs' sizes.")

    return parser.parse_args()


class Sequence(object):
    """ Represent a single fasta sequence. """
    def __init__(self, name, seq):
        self.name = name
        self.seq = seq

    def __str__(self):
        """ If sequence is longer than 10 symbols print first and last 5 of
            them. """
        returned_str = 'Name: {}\nSeq: '.format(self.name)
        if len(self.seq) <= 10:
            seq_str = self.seq
        else:
            seq_str = '{}...{}'.format(self.seq[0:5], self.seq[-5:])
        returned_str += seq_str

        return returned_str

    def write(self, file_handler):
        file_handler.write(self.name + '\n')
        file_handler.write(self.seq + '\n')

    def trim(self, freq):
        """ Trim the sequence from the both ends with the specified
            frequency. """
        # Trim the 10% of the sequence.
        left_clip = int(len(self.seq) * 0.1)
        if random.random() < freq:
            self.seq = self.seq[left_clip::]

        right_clip = int(len(self.seq) * 0.1)
        if random.random() < freq:
            self.seq = self.seq[:-right_clip:]

        return self


class AssemblyGenerator(object):
    """ Simulate assemblies from the sequence. """
    def __init__(self, num, reference_file, output_dir, trim_freq, sizes):
        self.num = num  # Number of generated assemblies.
        # Path to file with reference sequence.
        self.reference_file = reference_file
        self.sequences = []  # List with reference sequences.
        self.output_dir = output_dir  # Directory with generated assemblies.
        self.genome_size = 0  # Size of the reference genome.
        self.trim_freq = trim_freq  # Contig's trimming frequency.
        # A set of simulated contigs' sizes.
        self.sizes = [int(i) for i in sorted(sizes)]
        self.freqs = []  # List of frequencies for contigs occurence.
        # Helper sequence splitting invariants.
        self.max_contig = None
        self.sum_occurence = None
        self.accum_freqs = None

        sizes_str =\
            '[' + " ".join(["{:.0E}".format(i) for i in self.sizes]) + ']'
        logging.info("Will break reference with %s chunks.", sizes_str)

    def parse_file_with_reference(self):
        if not os.path.isfile(self.reference_file):
            logging.info("File '%s' with reference sequence doesn'e exist. "
                         "Exit now.", self.reference_file)
            sys.exit(1)

        logging.info("Parsing '%s' file with reference sequence...",
                     self.reference_file)
        # https://www.biostars.org/p/710/
        with open(self.reference_file) as reference_file:
            # ditch the boolean (x[0]) and just keep the header or sequence
            # since we know they alternate.
            faiter = (x[1] for x in groupby(reference_file,
                                            lambda line: line[0] == ">"))
            for header in faiter:
                header = header.next().strip()
                # join all sequence lines to one.
                seq = "".join(s.strip() for s in faiter.next())
                self.sequences.append(Sequence(header, seq))
                self.genome_size += len(seq)
                logging.info("Save '%s' sequence.", header)

        logging.info("Successfully parsed reference sequence. Found %d "
                     "contigs", len(self.sequences))

        # Calculate frequency for contigs occurence.
        self.freqs = [float(self.genome_size) / size for size in self.sizes]
        # self.freqs = [freq / sum(freqs) for freq in freqs]

        # Count occurence invariants.
        self.max_contig = self.sizes[-1]
        self.accum_freqs = [self.freqs[0]] * len(self.freqs)
        for i, freq in enumerate(self.freqs[1:]):
            self.accum_freqs[i + 1] = self.accum_freqs[i] + freq

        self.sum_occurence = self.accum_freqs[-1]

    def make_weighted_choice(self):
        """ random.choices equivalent. """
        coin = random.random() * self.sum_occurence

        size_id = bisect.bisect(self.accum_freqs, coin)
        return self.sizes[size_id]

    def choice_wrapper(self):
        while True:
            yield self.make_weighted_choice()

    def break_sequence(self, sequence, seq_id, assembly_id):
        """ Randomly split sequence into sequences. """
        seq = sequence.seq
        i = 1
        name_prefix = ">assembly{}_seq{}_".format(assembly_id, seq_id)
        for size_choice in self.choice_wrapper():
            if len(seq) < self.max_contig:
                break
            # Split sequences at long N parts.
            for contig_seq in filter(None, seq[:size_choice].split('N' * 10)):
                name = name_prefix + "part{}".format(i)
                yield Sequence(name, contig_seq).trim(self.trim_freq)
                i += 1
            seq = seq[size_choice::]

        # Deal with the remaining sequence part.
        for contig_seq in filter(None, seq.split('N' * 10)):
            name = name_prefix + "part{}".format(i)
            yield Sequence(name, contig_seq).trim(self.trim_freq)
            i += 1

    def generate_assembly(self, assembly_id):
        filepath =\
            os.path.join(self.output_dir, 'assembly_{}.fa'.format(assembly_id))
        logging.info("Writing assembly to '%s'..", os.path.basename(filepath))
        with open(filepath, 'w') as assembly_file:
            for seq_id, sequence in enumerate(self.sequences):
                for seq_part in self.break_sequence(sequence, seq_id + 1,
                                                    assembly_id):
                    seq_part.write(assembly_file)

        logging.info("Done.")


def setup_log():
    logging.basicConfig(
        filename=os.path.splitext(os.path.basename(__file__))[0] + '.log',
        level=logging.INFO, filemode='w')
    logging.getLogger().addHandler(logging.StreamHandler())


def main():
    args = parse_args()
    setup_log()

    assembly_generator = AssemblyGenerator(args.n, args.reference, args.output,
                                           args.trim, args.sizes)
    assembly_generator.parse_file_with_reference()

    for i in range(args.n):
        assembly_generator.generate_assembly(i + 1)


if __name__ == '__main__':
    main()
